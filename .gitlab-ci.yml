variables:
  CARGO_HOME: $CI_PROJECT_DIR/cargo
image: dagurval/rostrum-buildenv

cache:
  key: $CI_JOB_NAME
  paths:
    - $CARGO_HOME
    - target
    - apt-cache
  when: always

.cleanup_common:
  after_script:
    # Make sure cached dirs exist, so that CI doesn't warn about them.
    - mkdir -p $CARGO_HOME
    - mkdir -p target
    - mkdir -p apt-cache

    # Limit rust cache size to 500MB
    - cargo cache trim --limit 500M
    # Limit build files to at most 4GB
    - cargo sweep --maxsize 4000 > /dev/null
  artifacts:
    expire_in: 1 week

.test_common:
  extends: .cleanup_common
  before_script:
    - rustc --version
    - cargo --version
    - export PATH=$PATH:$CARGO_HOME/bin
  artifacts:
    reports:
      junit: results.xml

.install_dependencies_common:
  extends: .cleanup_common
  before_script:
    - export APT_CACHE_DIR=`pwd`/apt-cache && mkdir -pv $APT_CACHE_DIR
    - apt-get update
    - apt-get
      -o dir::cache::archives="$APT_CACHE_DIR" install
      -y --no-install-recommends
      clang=1:7.* cmake=3.* curl make > /dev/null

    - export PATH=$PATH:$CARGO_HOME/bin
    - rustc --version
    - cargo --version
    - cargo install cargo2junit
    - cargo install cargo-cache
    - cargo install cargo-sweep

.nightly_common:
  extends: .install_dependencies_common
  image: rustlang/rust:nightly-buster-slim

.msvr_common:
  extends: .install_dependencies_common
  image: rust:1.60-buster

.python_common:
  image: python:latest
  cache:
    key: $CI_JOB_NAME
    paths:
      - cache-pip
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR/cache-pip

.win64_common:
  extends: .cleanup_common
  variables:
    # Run tests using wine
    CARGO_TARGET_X86_64_PC_WINDOWS_GNU_RUNNER: wine64-stable
    WINEPATH: "/usr/lib/gcc/x86_64-w64-mingw32/10-posix/;/usr/x86_64-w64-mingw32/lib/"
  before_script:
    - apt-get update > /dev/null
    - apt-get install
        gcc-mingw-w64-x86-64-posix
        g++-mingw-w64-x86-64-posix -y > /dev/null
    - rustup target add x86_64-pc-windows-gnu

.aarch64_common:
  extends: .cleanup_common
  before_script:
    - apt-get update > /dev/null
    - apt-get install
        gcc-aarch64-linux-gnu
        g++-aarch64-linux-gnu
        libc6-dev-arm64-cross
        -y > /dev/null
    - rustup target add aarch64-unknown-linux-gnu
    - export CPATH=$CPATH:/usr/aarch64-linux-gnu/include
      # For some reason we need to tell rust what is the correct linker.
      # Fixes linker error "Relocations in generic ELF (EM: 183)"
    - export CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER=aarch64-linux-gnu-gcc

    # Set `variables: INSTALL_EMULATOR: 1` in job if emulator is needed
    - if [ $INSTALL_EMULATOR ]; then
        export CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_RUNNER=qemu-aarch64-static;
        export QEMU_LD_PREFIX=/usr/aarch64-linux-gnu/;
        apt-get install -y
            libc6-arm64-cross
            libstdc++6-arm64-cross
            qemu-user-static
            > /dev/null;
      fi



#
# Builds
#
build-stable-bch-linux64:
  extends: .cleanup_common
  script:
    - cargo build --release --locked --features=fail-on-warnings
      --target=x86_64-unknown-linux-gnu
  artifacts:
    paths:
      - target/x86_64-unknown-linux-gnu/release/rostrum

build-stable-nexa-linux64:
  extends: .cleanup_common
  script:
    - cargo build --release --locked --features=fail-on-warnings,nexa
      --target=x86_64-unknown-linux-gnu
  artifacts:
    paths:
      - target/x86_64-unknown-linux-gnu/release/rostrum

build-stable-bch-win64:
  extends: .win64_common
  script:
    - cargo build --release --locked
        --features=fail-on-warnings --target=x86_64-pc-windows-gnu
    - cp /usr/lib/gcc/x86_64-w64-mingw32/10-posix/*.dll target/x86_64-pc-windows-gnu/release/
    - cp /usr/x86_64-w64-mingw32/lib/*.dll target/x86_64-pc-windows-gnu/release/
  artifacts:
    paths:
      - target/x86_64-pc-windows-gnu/release/rostrum.exe
      - target/x86_64-pc-windows-gnu/release/*.dll

build-stable-nexa-win64:
  extends: .win64_common
  script:
    - cargo build --release --locked
        --features=fail-on-warnings,nexa --target=x86_64-pc-windows-gnu
    - cp /usr/lib/gcc/x86_64-w64-mingw32/10-posix/*.dll target/x86_64-pc-windows-gnu/release/
    - cp /usr/x86_64-w64-mingw32/lib/*.dll target/x86_64-pc-windows-gnu/release/
  artifacts:
    paths:
      - target/x86_64-pc-windows-gnu/release/rostrum.exe
      - target/x86_64-pc-windows-gnu/release/*.dll

build-stable-bch-linux-aarch64:
  extends: .aarch64_common
  script:
    - cargo build --release --locked
        --features=fail-on-warnings --target=aarch64-unknown-linux-gnu
  artifacts:
    paths:
      - target/aarch64-unknown-linux-gnu/release/rostrum

build-stable-nexa-linux-aarch64:
  extends: .aarch64_common
  script:
    - cargo build --release --locked
        --features=fail-on-warnings,nexa --target=aarch64-unknown-linux-gnu
  artifacts:
    paths:
      - target/aarch64-unknown-linux-gnu/release/rostrum


build-stable-no-default:
  extends: .cleanup_common
  before_script:
    - rustc --version
    - cargo --version
  script:
    - cargo build --features=fail-on-warnings --no-default-features
  artifacts:
    paths:
      - target/debug/rostrum
  rules:
    - if: $CI_COMMIT_BRANCH == "master"

build-nightly:
  extends: .nightly_common
  script:
    - cargo build --features=fail-on-warnings
  artifacts:
    paths:
      - target/debug/rostrum
  allow_failure: true

# The minimum rust version rostrum can be built with
# If bumped, also update doc/build.md
build-msrv:
  extends: .msvr_common
  script:
    - cargo build

build-docker:
  image: docker
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  cache: {}
  services:
    - docker:dind
  script:
    - docker build -t rostrum-app .

build-nightly-nexa:
  extends: .nightly_common
  script:
    - cargo build --features=fail-on-warnings,nexa
  artifacts:
    paths:
      - target/debug/rostrum
  allow_failure: true

build-docker-nexa:
  image: docker
  cache: {}
  services:
    - docker:dind
  script:
    - docker build --build-arg BUILD_FEATURES=nexa -t rostrum-app .
  rules:
    - if: $CI_COMMIT_BRANCH == "master"

#
# Linters
#
lint-code-style:
  extends: .cleanup_common
  script:
    - cargo fmt --all -- --check

lint-clippy:
  extends: .cleanup_common
  before_script:
    - rustup component add clippy
  script:
    - cargo clippy -- -D warnings

lint-clippy-nexa:
  extends: .cleanup_common
  before_script:
    - rustup component add clippy
  script:
    - cargo clippy --features=nexa -- -D warnings

lint-bloat:
  extends: .cleanup_common
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  script:
    # Biggest functions
    - cargo bloat --release

    # Biggest dependencies
    - cargo bloat --release --crates

    # Longest to compile
    - cargo bloat --time -j 1

lint-udeps:
  extends: .nightly_common
  script:
    - apt-get
      -o dir::cache::archives="$APT_CACHE_DIR" install
      -y --no-install-recommends
      pkg-config libssl-dev > /dev/null
    - cargo install cargo-udeps --locked
    - cargo +nightly udeps

lint-audit:
  extends: .cleanup_common
  before_script:
    - apt-get update > /dev/null
    - apt-get
      -o dir::cache::archives="$APT_CACHE_DIR" install
      -y --no-install-recommends
      pkg-config libssl-dev > /dev/null
    - cargo install cargo-audit
  script:
    # 'RUSTSEC-2020-0071' - Issue in time dependency of stderrlog
    - cargo audit
      --ignore RUSTSEC-2020-0071

#
# Tests
#
test-stable-bch-linux64:
  extends: .test_common
  script:
    - cargo test
        --target=x86_64-unknown-linux-gnu
        --features=fail-on-warnings
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-stable-nexa-linux64:
  extends: .test_common
  script:
    - cargo test
        --target=x86_64-unknown-linux-gnu
        --features=fail-on-warnings,nexa
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-stable-bch-win64:
  extends:
    - .test_common
    - .win64_common
  script:
    - apt-get install wine64 -y > /dev/null
    - cargo test
        --target=x86_64-pc-windows-gnu
        --features=fail-on-warnings
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-stable-nexa-win64:
  extends:
    - .test_common
    - .win64_common
  script:
    - apt-get install wine64 -y > /dev/null
    - cargo test
        --target=x86_64-pc-windows-gnu
        --features=fail-on-warnings,nexa
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-stable-bch-linux-aarch64:
  variables:
    INSTALL_EMULATOR: 1
  extends:
    - .test_common
    - .aarch64_common
  script:
    - cargo test
        --target=aarch64-unknown-linux-gnu
        --features=fail-on-warnings
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-stable-nexa-linux-aarch64:
  variables:
    INSTALL_EMULATOR: 1
  extends:
    - .test_common
    - .aarch64_common
  script:
    - cargo test
        --target=aarch64-unknown-linux-gnu
        --features=fail-on-warnings,nexa
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-stable-no-default:
  extends: .test_common
  script:
    - cargo test
        --features=fail-on-warnings
        --no-default-features
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml
  rules:
    - if: $CI_COMMIT_BRANCH == "master"

test-nightly-bch:
  extends:
    - .test_common
    - .nightly_common
  script:
    - cargo test
        --features=fail-on-warnings
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml
  allow_failure: true

test-nightly-nexa:
  extends:
    - .test_common
    - .nightly_common
  script:
    - cargo test
        --features=fail-on-warnings,nexa
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml
  allow_failure: true

bench-nightly:
  extends:
    - .nightly_common
  script:
    - cargo bench
        --features=fail-on-warnings,with-benchmarks

  # Allow failure because it uses nightly
  allow_failure: true

test-msrv:
  extends:
    - .test_common
    - .msvr_common
  script:
    - cargo test
        --features=fail-on-warnings
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-qa-stable-nexa:
  extends: .python_common
  dependencies:
    - build-stable-nexa-linux64
  needs: [build-stable-nexa-linux64]
  before_script:
    - pip install -q psutil
    - pip install -q zmq
  script:
    # Download latest build from Nexa CI/CD
    - wget --quiet -O nexa-build.zip
      "https://gitlab.com/nexa/nexa/-/jobs/artifacts/dev/download?job=build-ubuntu"
    - unzip -x nexa-build.zip "build/src/nexad" -d nexa > /dev/null

    # Download QA tests
    - wget --quiet https://gitlab.com/nexa/nexa/-/archive/dev/nexa-dev.zip
    - unzip -x nexa-dev.zip -d nexa > /dev/null
    - mv nexa/nexa-dev/qa nexa/build/qa

    # Run tests
    - export QA_DIR=`pwd`/nexa/build/src
    - echo "SRCDIR=\"$QA_DIR/..\""
        > nexa/build/qa/pull-tester/tests_config.py
    - echo "BUILDDIR=\"$QA_DIR\""
        >> nexa/build/qa/pull-tester/tests_config.py
    - echo "EXEEXT=\"\""
        >> nexa/build/qa/pull-tester/tests_config.py
    - echo "ENABLE_WALLET=1"
        >> nexa/build/qa/pull-tester/tests_config.py
    - echo "ENABLE_UTILS=1"
        >> nexa/build/qa/pull-tester/tests_config.py
    - echo "ENABLE_NEXAD=1"
        >> nexa/build/qa/pull-tester/tests_config.py
    - echo "ENABLE_ZMQ=1"
        >> nexa/build/qa/pull-tester/tests_config.py
    - RUST_BACKTRACE=1 PYTHON_DEBUG=INFO NEXAD=`pwd`/nexa/build/src/nexad
      python3 ./nexa/build/qa/pull-tester/rpc-tests.py
      -parallel=1
      --electrum-only --no-ipv6-rpc-listen --gitlab
      --electrum.exec=`pwd`/target/x86_64-unknown-linux-gnu/release/rostrum
  artifacts:
    paths:
      - ./nexa/build/qa/qa_tests
    when: always

#
# Published artifacts
#
generate-rustdoc:
  extends: .cleanup_common
  script:
    - cargo doc --no-deps -p rostrum
    - cargo rustdoc -p rostrum
  artifacts:
    paths:
      - target/doc

generate-mkdocs:
  extends: .python_common
  before_script:
    - pip install -q mkdocs
    - pip install -q mkdocs-material
    - pip install -q toml
  script:
    - ./contrib/build-docs.py
  artifacts:
    paths:
      - build-docs

archive-builds-bch:
  extends: .python_common
  needs:
    - build-stable-bch-linux64
    - build-stable-bch-win64
    - build-stable-bch-linux-aarch64
  before_script:
    - pip install -q toml
  script:
    - apt-get update && apt-get install zip -y > /dev/null
    - ROSTRUM_VERSION=`./contrib/read-version-number.py`
    - GIT_HEAD=`git rev-parse --short HEAD`
    - ROSTRUM_VERSION="$ROSTRUM_VERSION-$GIT_HEAD"
    - mkdir -p output
    - gzip -c target/x86_64-unknown-linux-gnu/release/rostrum
        > "output/rostrum-bitcoincash-linux-x86-64-$ROSTRUM_VERSION.gz"
    - gzip -c target/aarch64-unknown-linux-gnu/release/rostrum
        > "output/rostrum-bitcoincash-linux-arm64-$ROSTRUM_VERSION.gz"
    - zip
        "output/rostrum-bitcoincash-windows-x86-64-$ROSTRUM_VERSION.zip"
        target/x86_64-pc-windows-gnu/release/rostrum.exe
        target/x86_64-pc-windows-gnu/release/*.dll
  artifacts:
    paths:
      - output

archive-builds-nexa:
  extends: .python_common
  needs:
    - build-stable-nexa-linux64
    - build-stable-nexa-win64
    - build-stable-nexa-linux-aarch64
  before_script:
    - pip install -q toml
  script:
    - apt-get update && apt-get install zip -y > /dev/null
    - ROSTRUM_VERSION=`./contrib/read-version-number.py`
    - GIT_HEAD=`git rev-parse --short HEAD`
    - ROSTRUM_VERSION="$ROSTRUM_VERSION-$GIT_HEAD"
    - mkdir -p output
    - gzip -c target/x86_64-unknown-linux-gnu/release/rostrum
        > "output/rostrum-nexa-linux-x86-64-$ROSTRUM_VERSION.gz"
    - gzip -c target/aarch64-unknown-linux-gnu/release/rostrum
        > "output/rostrum-nexa-linux-arm64-$ROSTRUM_VERSION.gz"
    - zip
        "output/rostrum-nexa-windows-x86-64-$ROSTRUM_VERSION.zip"
        target/x86_64-pc-windows-gnu/release/rostrum.exe
        target/x86_64-pc-windows-gnu/release/*.dll
  artifacts:
    paths:
      - output


pages:
  image: alpine:latest
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  dependencies:
    - generate-rustdoc
    - generate-mkdocs
    - archive-builds-bch
    - archive-builds-nexa
  needs: [
    generate-rustdoc, generate-mkdocs,
    archive-builds-bch, archive-builds-nexa
  ]
  before_script:
    apk update && apk add tree
  script:
    # from the generate-mkdocs job
    - mv build-docs public
    # from the generate-rustdoc job
    - mv target/doc public/reference
    # from the archive-builds job
    - mv output public/nightly
    # create directory listing
    - (cd public/nightly;
        tree -H '.' -L 1 --noreport --charset utf-8 -o index.html)
  artifacts:
    paths:
      - public
  cache: {}
