use crate::chain::Chain;
use crate::chaindef::BlockHash;

use crate::chaindef::Transaction;
use crate::errors::rpc_invalid_params;
use crate::errors::rpc_other;
use crate::errors::ConnectionError;
use crate::metrics;
use bitcoin_hashes::Hash;
use bitcoincash::consensus::encode::{deserialize, serialize};
use bitcoincash::hash_types::Txid;
use bitcoincash::hashes::hex::FromHex;
use bitcoincash::network::constants::Network;
use serde_json::{from_str, from_value, Map, Value};
use std::collections::{HashMap, HashSet};
use std::io::{BufRead, BufReader, Lines, Write};
use std::net::{SocketAddr, TcpStream};
use std::path::{Path, PathBuf};
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, Mutex};
use std::time::Duration;

use crate::cache::BlockTxIDsCache;
use crate::chaindef::Block;
use crate::chaindef::BlockHeader;

use crate::metrics::Metrics;
#[cfg(feature = "nexa")]
use crate::nexa::hash_types::TxIdem;
use crate::rpc::encoding::{blockhash_to_hex, hex_to_blockhash, hex_to_txid, txid_to_hex};
use crate::signal::Waiter;
use anyhow::{Context, Result};

fn header_from_value(value: Value) -> Result<BlockHeader> {
    let header_hex = value
        .as_str()
        .context(format!("non-string header: {}", value))?;
    let header_bytes = hex::decode(header_hex).context("non-hex header")?;
    deserialize(&header_bytes).context(format!("failed to parse header {}", header_hex))
}

fn block_from_value(value: Value) -> Result<Block> {
    let block_hex = value.as_str().context("non-string block")?;
    let block_bytes = hex::decode(block_hex).context("non-hex block")?;
    deserialize(&block_bytes).context(format!("failed to parse block {}", block_hex))
}

fn tx_from_value(value: Value) -> Result<Transaction> {
    let tx_hex = value.as_str().context("non-string tx")?;
    let tx_bytes = hex::decode(tx_hex).context("non-hex tx")?;
    deserialize(&tx_bytes).context(format!("failed to parse tx {}", tx_hex))
}

/// Parse JSONRPC error code, if exists.
fn parse_error_code(err: &Value) -> Option<i64> {
    if err.is_null() {
        return None;
    }
    err.as_object()?.get("code")?.as_i64()
}

fn check_error_code(reply_obj: &Map<String, Value>, method: &str) -> Result<()> {
    if let Some(err) = reply_obj.get("error") {
        if let Some(code) = parse_error_code(err) {
            match code {
                // RPC_IN_WARMUP -> retry by later reconnection
                -28 => bail!(ConnectionError {
                    msg: err.to_string()
                }),
                // RPC_INVALID_ADDRESS_OR_KEY
                -5 => bail!(rpc_invalid_params(err.to_string())),
                _ => bail!(rpc_other(format!(
                    "Call '{}' to full node failed: {}",
                    method, err
                ))),
            }
        }
    }
    Ok(())
}

fn parse_jsonrpc_reply(mut reply: Value, method: &str, expected_id: u64) -> Result<Value> {
    if let Some(reply_obj) = reply.as_object_mut() {
        check_error_code(reply_obj, method)?;
        let id = reply_obj
            .get("id")
            .context(format!("no id in reply: {:?}", reply_obj))?
            .clone();
        if id != expected_id {
            bail!(
                "wrong {} response id {}, expected {}",
                method,
                id,
                expected_id
            );
        }
        if let Some(result) = reply_obj.get_mut("result") {
            return Ok(result.take());
        }
        bail!("no result in reply: {:?}", reply_obj);
    }
    bail!("non-object reply: {:?}", reply);
}

#[derive(Serialize, Deserialize, Debug)]
struct BlockchainInfo {
    chain: String,
    blocks: u32,
    headers: u32,
    verificationprogress: f64,
    bestblockhash: String,
    pruned: bool,
    initialblockdownload: bool,
}

#[derive(Serialize, Deserialize, Debug)]
struct NetworkInfo {
    version: u64,
    subversion: String,
    relayfee: f64, // in BTC
}

pub struct MempoolEntry {
    fee: u64,   // in satoshis
    vsize: u32, // in virtual bytes (= weight/4)
    fee_per_vbyte: f32,
}

impl MempoolEntry {
    pub(crate) fn new(fee: u64, vsize: u32) -> MempoolEntry {
        MempoolEntry {
            fee,
            vsize,
            fee_per_vbyte: fee as f32 / vsize as f32,
        }
    }

    pub fn fee_per_vbyte(&self) -> f32 {
        self.fee_per_vbyte
    }

    pub fn fee(&self) -> u64 {
        self.fee
    }

    pub fn vsize(&self) -> u32 {
        self.vsize
    }
}

pub trait CookieGetter: Send + Sync {
    fn get(&self) -> Result<Vec<u8>>;
}

struct Connection {
    tx: TcpStream,
    rx: Lines<BufReader<TcpStream>>,
    cookie_getter: Arc<dyn CookieGetter>,
    addr: SocketAddr,
    signal: Waiter,
}

fn tcp_connect(addr: SocketAddr, signal: &Waiter) -> Result<TcpStream> {
    loop {
        match TcpStream::connect(addr) {
            Ok(conn) => return Ok(conn),
            Err(err) => {
                warn!("failed to connect daemon at {}: {}", addr, err);
                signal.wait(Duration::from_secs(3))?;
                continue;
            }
        }
    }
}

impl Connection {
    fn new(
        addr: SocketAddr,
        cookie_getter: Arc<dyn CookieGetter>,
        signal: Waiter,
    ) -> Result<Connection> {
        let conn = tcp_connect(addr, &signal)?;
        let reader = BufReader::new(
            conn.try_clone()
                .context(format!("failed to clone {:?}", conn))?,
        );
        Ok(Connection {
            tx: conn,
            rx: reader.lines(),
            cookie_getter,
            addr,
            signal,
        })
    }

    fn reconnect(&self) -> Result<Connection> {
        Connection::new(self.addr, self.cookie_getter.clone(), self.signal.clone())
    }

    fn send(&mut self, request: &str) -> Result<()> {
        let cookie = &self.cookie_getter.get()?;
        let msg = format!(
            "POST / HTTP/1.1\nAuthorization: Basic {}\nContent-Length: {}\n\n{}",
            base64::encode(cookie),
            request.len(),
            request,
        );
        self.tx.write_all(msg.as_bytes()).context({
            ConnectionError {
                msg: "disconnected from daemon while sending".to_string(),
            }
        })
    }

    fn recv(&mut self) -> Result<String> {
        // TODO: use proper HTTP parser.
        let mut in_header = true;
        let mut contents: Option<String> = None;
        let iter = self.rx.by_ref();
        let status = iter
            .next()
            .context({
                ConnectionError {
                    msg: "disconnected from daemon while receiving".to_string(),
                }
            })?
            .context(ConnectionError {
                msg: "failed to read status".to_string(),
            })?;
        let mut headers = HashMap::new();
        for line in iter {
            let line = line.context(ConnectionError {
                msg: "failed to read".to_string(),
            })?;
            if line.is_empty() {
                in_header = false; // next line should contain the actual response.
            } else if in_header {
                let parts: Vec<&str> = line.splitn(2, ": ").collect();
                if parts.len() == 2 {
                    headers.insert(parts[0].to_owned(), parts[1].to_owned());
                } else {
                    warn!("invalid header: {:?}", line);
                }
            } else {
                contents = Some(line);
                break;
            }
        }

        let contents = contents.context(ConnectionError {
            msg: "no reply from daemon".to_string(),
        })?;
        let contents_length: &str = headers
            .get("Content-Length")
            .context(format!("Content-Length is missing: {:?}", headers))?;
        let contents_length: usize = contents_length
            .parse()
            .context(format!("invalid Content-Length: {:?}", contents_length))?;

        let expected_length = contents_length - 1; // trailing EOL is skipped
        if expected_length != contents.len() {
            bail!(ConnectionError {
                msg: format!("expected {} bytes, got {}", expected_length, contents.len())
            });
        }

        Ok(if status == "HTTP/1.1 200 OK" {
            contents
        } else if status == "HTTP/1.1 500 Internal Server Error" {
            warn!("HTTP status: {}", status);
            contents // the contents should have a JSONRPC error field
        } else {
            bail!(
                "request failed {:?}: {:?} = {:?}",
                status,
                headers,
                contents
            );
        })
    }
}

struct Counter {
    value: AtomicU64,
}

impl Counter {
    fn new() -> Self {
        Counter { value: 0.into() }
    }

    fn next(&self) -> u64 {
        // fetch_add() returns previous value, we want current one
        self.value.fetch_add(1, Ordering::Relaxed) + 1
    }
}

pub struct Daemon {
    daemon_dir: PathBuf,
    network: Network,
    conn: Mutex<Connection>,
    message_id: Counter, // for monotonic JSONRPC 'id'
    signal: Waiter,
    blocktxids_cache: Arc<BlockTxIDsCache>,

    // monitoring
    latency: prometheus::HistogramVec,
    size: prometheus::HistogramVec,
}

impl Daemon {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        daemon_dir: &Path,
        daemon_rpc_addr: SocketAddr,
        cookie_getter: Arc<dyn CookieGetter>,
        network: Network,
        signal: Waiter,
        blocktxids_cache: Arc<BlockTxIDsCache>,
        metrics: &Metrics,
    ) -> Result<Daemon> {
        let daemon = Daemon {
            daemon_dir: daemon_dir.to_path_buf(),
            network,
            conn: Mutex::new(Connection::new(
                daemon_rpc_addr,
                cookie_getter,
                signal.clone(),
            )?),
            message_id: Counter::new(),
            blocktxids_cache,
            signal: signal.clone(),
            latency: metrics.histogram_vec(
                "rostrum_daemon_rpc",
                "Bitcoind RPC latency (in seconds)",
                &["method"],
                metrics::default_duration_buckets(),
            ),
            size: metrics.histogram_vec(
                "rostrum_daemon_bytes",
                "Bitcoind RPC size (in bytes)",
                &["method", "dir"],
                metrics::default_size_buckets(),
            ),
        };
        let network_info = daemon.getnetworkinfo()?;
        info!("{:?}", network_info);
        if network_info.version < 16_00_00 {
            bail!(
                "{} is not supported - please use bitcoind 0.16+",
                network_info.subversion,
            )
        }
        let blockchain_info = daemon.getblockchaininfo()?;
        info!("{:?}", blockchain_info);
        if blockchain_info.pruned {
            bail!("pruned node is not supported (use '-prune=0' bitcoind flag)".to_owned())
        }
        loop {
            let info = daemon.getblockchaininfo()?;
            if !info.initialblockdownload {
                break;
            }
            if network == Network::Regtest && info.headers == info.blocks {
                break;
            }
            warn!(
                "wait until IBD is over: headers={} blocks={} progress={}",
                info.headers, info.blocks, info.verificationprogress
            );
            signal.wait(Duration::from_secs(3))?;
        }
        Ok(daemon)
    }

    pub fn reconnect(&self) -> Result<Daemon> {
        Ok(Daemon {
            daemon_dir: self.daemon_dir.clone(),
            network: self.network,
            conn: Mutex::new(self.conn.lock().unwrap().reconnect()?),
            message_id: Counter::new(),
            signal: self.signal.clone(),
            blocktxids_cache: Arc::clone(&self.blocktxids_cache),
            latency: self.latency.clone(),
            size: self.size.clone(),
        })
    }

    fn call_jsonrpc(&self, method: &str, request: &Value) -> Result<Value> {
        let mut conn = self.conn.lock().unwrap();
        let timer = self.latency.with_label_values(&[method]).start_timer();
        let request = request.to_string();
        conn.send(&request)?;
        self.size
            .with_label_values(&[method, "send"])
            .observe(request.len() as f64);
        let response = conn.recv()?;
        let result: Value = from_str(&response).context("invalid JSON")?;
        timer.observe_duration();
        self.size
            .with_label_values(&[method, "recv"])
            .observe(response.len() as f64);
        Ok(result)
    }

    fn handle_request_batch(&self, method: &str, params_list: &[Value]) -> Result<Vec<Value>> {
        let id = self.message_id.next();
        let reqs = params_list
            .iter()
            .map(|params| json!({"method": method, "params": params, "id": id}))
            .collect();
        let mut results = vec![];
        let mut replies = self.call_jsonrpc(method, &reqs)?;
        if let Some(replies_vec) = replies.as_array_mut() {
            for reply in replies_vec {
                results.push(parse_jsonrpc_reply(reply.take(), method, id)?)
            }
            return Ok(results);
        }
        bail!("non-array replies: {:?}", replies);
    }

    fn retry_request_batch(&self, method: &str, params_list: &[Value]) -> Result<Vec<Value>> {
        loop {
            match self.handle_request_batch(method, params_list) {
                Err(e) => {
                    if let Some(e) = e.downcast_ref::<ConnectionError>() {
                        warn!("reconnecting to bitcoind: {}", e.msg);
                        self.signal.wait(Duration::from_secs(1))?;
                        let mut conn = self.conn.lock().unwrap();
                        *conn = conn.reconnect()?;
                        continue;
                    }
                    return Err(e);
                }
                Ok(result) => return Ok(result),
            }
        }
    }

    fn request(&self, method: &str, params: Value) -> Result<Value> {
        let mut values = self.retry_request_batch(method, &[params])?;
        assert_eq!(values.len(), 1);
        Ok(values.remove(0))
    }

    fn requests(&self, method: &str, params_list: &[Value]) -> Result<Vec<Value>> {
        self.retry_request_batch(method, params_list)
    }

    // bitcoind JSONRPC API:

    fn getblockchaininfo(&self) -> Result<BlockchainInfo> {
        let info: Value = self.request("getblockchaininfo", json!([]))?;
        from_value(info).context("invalid blockchain info")
    }

    fn getnetworkinfo(&self) -> Result<NetworkInfo> {
        let info: Value = self.request("getnetworkinfo", json!([]))?;
        from_value(info).context("invalid network info")
    }

    pub fn get_subversion(&self) -> Result<String> {
        Ok(self.getnetworkinfo()?.subversion)
    }

    pub fn get_relayfee(&self) -> Result<f64> {
        Ok(self.getnetworkinfo()?.relayfee)
    }

    pub fn getbestblockhash(&self) -> Result<BlockHash> {
        hex_to_blockhash(&self.request("getbestblockhash", json!([]))?).context("invalid blockhash")
    }

    pub fn getblockheader(&self, blockhash: &BlockHash) -> Result<(BlockHeader, u64)> {
        let header = header_from_value(self.request(
            "getblockheader",
            json!([blockhash_to_hex(blockhash), /*verbose=*/ false]),
        )?)?;
        let height = self.get_header_height(&header)?;
        Ok((header, height))
    }

    #[cfg(feature = "nexa")]
    pub(crate) fn get_header_height(&self, header: &BlockHeader) -> Result<u64> {
        Ok(header.height())
    }

    #[cfg(not(feature = "nexa"))]
    pub(crate) fn get_header_height(&self, header: &BlockHeader) -> Result<u64> {
        let response = self.request(
            "getblockheader",
            json!([
                blockhash_to_hex(&header.block_hash()),
                true /* verbose */
            ]),
        )?;
        response
            .get("height")
            .context("height missing for header")?
            .as_u64()
            .context("height was not numerical")
    }

    pub fn getblockheaders(&self, heights: &[usize]) -> Result<Vec<(BlockHeader, u64)>> {
        let heights_json: Vec<Value> = heights.iter().map(|height| json!([height])).collect();
        let params_list: Vec<Value> = self
            .requests("getblockhash", &heights_json)?
            .into_iter()
            .map(|hash| json!([hash, /*verbose=*/ false]))
            .collect();
        let result = self
            .requests("getblockheader", &params_list)?
            .into_iter()
            .zip(heights.iter())
            .map(|(response, height)| {
                let header = header_from_value(response)?;
                Ok((header, *height as u64))
            })
            .collect();
        result
    }

    pub fn getblock(&self, blockhash: &BlockHash) -> Result<Block> {
        let block = block_from_value(self.request(
            "getblock",
            json!([blockhash_to_hex(blockhash), /*verbose=*/ false]),
        )?)?;
        assert_eq!(block.block_hash(), *blockhash);
        Ok(block)
    }

    fn load_blocktxids(&self, blockhash: &BlockHash) -> Result<Vec<Txid>> {
        let block = self.request(
            "getblock",
            json!([blockhash_to_hex(blockhash), /*verbose=*/ 1]),
        )?;

        block
            .get("tx")
            .or_else(|| block.get("txid")) // NEXA variant of the keyword
            .context("block missing txids")?
            .as_array()
            .context("invalid block txids")?
            .iter()
            .map(hex_to_txid)
            .collect::<Result<Vec<Txid>>>()
    }

    pub fn getblocktxids(&self, blockhash: &BlockHash) -> Result<Vec<Txid>> {
        self.blocktxids_cache
            .get_or_else(blockhash, || self.load_blocktxids(blockhash))
    }

    pub fn gettransaction(&self, txid: &Txid, blockhash: Option<BlockHash>) -> Result<Transaction> {
        let mut args = json!([txid_to_hex(txid), /*verbose=*/ false]);
        if let Some(blockhash) = blockhash {
            args.as_array_mut()
                .unwrap()
                .push(json!(blockhash_to_hex(&blockhash)));
        }
        tx_from_value(self.request("getrawtransaction", args)?)
    }

    pub fn gettransaction_raw(
        &self,
        txid: &Txid,
        blockhash: Option<&BlockHash>,
        verbose: bool,
    ) -> Result<Value> {
        let mut args = json!([txid_to_hex(txid), verbose]);
        if let Some(blockhash) = blockhash {
            args.as_array_mut()
                .unwrap()
                .push(json!(blockhash_to_hex(blockhash)));
        }
        self.request("getrawtransaction", args)
    }

    fn txids_to_hashset(&self, txids: Value) -> Result<HashSet<Txid>> {
        let mut result = HashSet::new();
        for value in txids.as_array().context("non-array result")? {
            result.insert(hex_to_txid(value).context("invalid txid")?);
        }
        Ok(result)
    }

    #[cfg(feature = "nexa")]
    pub fn getmempooltxids(&self) -> Result<HashSet<Txid>> {
        let txids: Value = self.request(
            "getrawtxpool",
            json!([/*verbose=*/ false, /* id or idem */ "id"]),
        )?;
        self.txids_to_hashset(txids)
    }

    #[cfg(not(feature = "nexa"))]
    pub fn getmempooltxids(&self) -> Result<HashSet<Txid>> {
        let txids: Value = self.request("getrawmempool", json!([/*verbose=*/ false]))?;
        self.txids_to_hashset(txids)
    }

    fn to_mempoolentry(&self, entry: Value) -> Result<MempoolEntry> {
        let fee = (entry
            .get("fee")
            .context("missing fee")?
            .as_f64()
            .context("non-float fee")?
            * 100_000_000f64) as u64;
        let vsize = entry
            .get("size")
            .or_else(|| entry.get("vsize")) // (https://github.com/bitcoin/bitcoin/pull/15637)
            .context("missing vsize")?
            .as_u64()
            .context("non-integer vsize")? as u32;
        Ok(MempoolEntry::new(fee, vsize))
    }

    #[cfg(not(feature = "nexa"))]
    pub fn getmempoolentry(&self, txid: &Txid) -> Result<MempoolEntry> {
        let entry = self.request("getmempoolentry", json!([txid_to_hex(txid)]))?;
        self.to_mempoolentry(entry)
    }

    #[cfg(feature = "nexa")]
    pub fn getmempoolentry(&self, txid: &Txid) -> Result<MempoolEntry> {
        let entry = self.request("gettxpoolentry", json!([txid_to_hex(txid)]))?;
        self.to_mempoolentry(entry)
    }

    #[cfg(not(feature = "nexa"))]
    pub fn broadcast(&self, tx: &Transaction) -> Result<Txid> {
        let tx = hex::encode(serialize(tx));
        let txid = self.request("sendrawtransaction", json!([tx]))?;
        Txid::from_hex(txid.as_str().context("non-string txid")?).context("failed to parse txid")
    }
    #[cfg(feature = "nexa")]
    pub fn broadcast(&self, tx: &Transaction) -> Result<TxIdem> {
        let tx = hex::encode(serialize(tx));
        let txidem = self.request("sendrawtransaction", json!([tx]))?;
        TxIdem::from_hex(txidem.as_str().context("non-string txidem")?)
            .context("failed to parse txidem")
    }

    fn get_all_headers(&self, tip: &BlockHash) -> Result<Vec<(BlockHeader, u64)>> {
        let info: Value = self.request("getblockheader", json!([blockhash_to_hex(tip)]))?;
        let tip_height = info
            .get("height")
            .expect("missing height")
            .as_u64()
            .expect("non-numeric height") as usize;
        let all_heights: Vec<usize> = (0..=tip_height).collect();
        let chunk_size = 100_000;
        let mut result = vec![];
        let null_hash = BlockHash::all_zeros();
        for heights in all_heights.chunks(chunk_size) {
            trace!("downloading {} block headers", heights.len());
            let mut headers = self.getblockheaders(heights)?;
            assert!(headers.len() == heights.len());
            result.append(&mut headers);
        }

        let mut blockhash = null_hash;
        let mut prev_header: Option<BlockHeader> = None;
        #[allow(clippy::clone_on_copy)]
        for (header, _height) in &result {
            assert_eq!(
                header.prev_blockhash,
                blockhash,
                "Prev block hex: {}, this block hex: {}",
                hex::encode(&serialize(&prev_header.unwrap())),
                hex::encode(&serialize(&header)),
            );
            blockhash = header.block_hash();
            prev_header = Some(header.clone());
        }
        assert_eq!(blockhash, *tip);
        Ok(result)
    }

    // Returns a list of BlockHeaders in ascending height (i.e. the tip is last).
    pub fn get_new_headers(
        &self,
        chain: &Chain,
        bestblockhash: &BlockHash,
    ) -> Result<Vec<(BlockHeader, u64)>> {
        // Iterate back over headers until known blockash is found:
        if chain.height() == 0 {
            return self.get_all_headers(bestblockhash);
        }
        debug!(
            "downloading new block headers ({} already indexed) from {}",
            chain.height(),
            bestblockhash,
        );
        let mut new_headers: Vec<(BlockHeader, u64)> = vec![];
        let null_hash = BlockHash::all_zeros();
        let mut blockhash = *bestblockhash;
        while blockhash != null_hash {
            if chain.contains(&blockhash) {
                break;
            }
            let header = self
                .getblockheader(&blockhash)
                .context(format!("failed to get {} header", blockhash))?;
            blockhash = header.0.prev_blockhash;
            new_headers.push(header);
        }
        trace!("downloaded {} block headers", new_headers.len());
        new_headers.reverse(); // so the tip is the last vector entry
        Ok(new_headers)
    }

    pub fn get_genesis(&self) -> Result<Block> {
        let response = self.request("getblock", json!([0, false]))?;
        block_from_value(response)
    }

    pub fn get_block_count(&self) -> Result<u64> {
        self.request("getblockcount", json!([]))?
            .as_u64()
            .context("expected blockheight")
    }
}
