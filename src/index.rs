use crate::cashaccount::CashAccountParser;
use crate::chain::Chain;
use crate::chain::NewHeader;
use crate::chaindef::Block;
use crate::chaindef::BlockHash;
use crate::chaindef::BlockHeader;
use crate::chaindef::ScriptHash;
use crate::chaindef::{Transaction, TxIn};
use crate::daemon::Daemon;
use crate::encode::compute_output_hash;
use crate::indexes::headerindex::HeaderRow;
use crate::indexes::heightindex::HeightIndexRow;
use crate::indexes::inputindex::InputIndexRow;
use crate::indexes::outputindex::OutputIndexRow;
use crate::indexes::scripthashindex::OutputFlags;
use crate::indexes::scripthashindex::ScriptHashIndexRow;
use crate::indexes::DBRow;
use crate::metrics;
use crate::metrics::Metrics;
use crate::signal::Waiter;
use crate::store;
use crate::store::DBStore;
use crate::store::{ReadStore, Row, WriteStore};
use crate::thread::spawn;
use crate::util::SyncChannel;
use anyhow::Context;
use anyhow::Result;
use bitcoin_hashes::hex::ToHex;
use bitcoin_hashes::Hash;
use bitcoincash::consensus::encode::{deserialize, serialize};
use rayon::prelude::*;
use std::convert::TryInto;
use std::sync::Arc;

struct Stats {
    update_duration: prometheus::HistogramVec,
    blocks: prometheus::IntCounter,
    txns: prometheus::IntCounter,
    vsize: prometheus::IntCounter,
    height: prometheus::IntGauge,
}

impl Stats {
    fn new(metrics: &Metrics) -> Stats {
        Stats {
            update_duration: metrics.histogram_vec(
                "rostrum_update_duration",
                "Indexing update duration (in seconds)",
                &["step"],
                metrics::default_duration_buckets(),
            ),
            blocks: metrics.counter_int(prometheus::Opts::new(
                "rostrum_index_blocks",
                "# of indexed blocks",
            )),
            txns: metrics.counter_int(prometheus::Opts::new(
                "rostrum_index_txns",
                "# of indexed transactions",
            )),
            vsize: metrics.counter_int(prometheus::Opts::new(
                "rostrum_index_vsize",
                "# of indexed vbytes",
            )),
            height: metrics.gauge_int(prometheus::Opts::new(
                "rostrum_index_height",
                "Last indexed block's height",
            )),
        }
    }

    fn update(&self, block: &Block, height: usize) {
        self.blocks.inc();
        self.txns.inc_by(block.txdata.len() as u64);
        for tx in &block.txdata {
            self.vsize.inc_by(tx.size() as u64);
        }
        self.update_height(height);
    }

    fn update_height(&self, height: usize) {
        self.height.set(height as i64);
    }

    fn start_timer(&self, step: &str) -> prometheus::HistogramTimer {
        self.update_duration
            .with_label_values(&[step])
            .start_timer()
    }

    fn observe_chain(&self, chain: &Chain) {
        self.height.set(chain.height() as i64)
    }
}

#[cfg(not(feature = "nexa"))]
#[inline]
fn is_coinbase_input(input: &TxIn) -> bool {
    use bitcoincash::Txid;
    let null_hash: Txid = Txid::all_zeros();
    input.previous_output.txid == null_hash
}
#[cfg(feature = "nexa")]
#[inline]
fn is_coinbase_input(_input: &TxIn) -> bool {
    // nexa does not have inputs in coinbase
    false
}
#[cfg(not(feature = "nexa"))]
#[inline]
fn get_output_hash(input: &TxIn) -> [u8; 32] {
    compute_output_hash(&input.previous_output.txid, input.previous_output.vout)
        .to_vec()
        .try_into()
        .unwrap()
}
#[cfg(feature = "nexa")]
#[inline]
fn get_output_hash(input: &TxIn) -> [u8; 32] {
    input.previous_output.hash.to_vec().try_into().unwrap()
}

#[cfg(feature = "nexa")]
fn index_outputs(tx: &Transaction) -> impl '_ + ParallelIterator<Item = Row> {
    let txid = tx.txid();
    tx.output.par_iter().enumerate().map(move |(i, output)| {
        OutputIndexRow::new(
            &txid,
            &compute_output_hash(&tx.txidem(), i as u32),
            output.value,
            i as u32,
        )
        .to_row()
    })
}

#[cfg(not(feature = "nexa"))]
fn index_outputs(tx: &Transaction) -> impl '_ + ParallelIterator<Item = Row> {
    let txid = tx.txid();
    tx.output.par_iter().enumerate().map(move |(i, output)| {
        OutputIndexRow::new(
            &txid,
            &compute_output_hash(&txid, i as u32),
            output.value,
            i as u32,
        )
        .to_row()
    })
}

#[cfg(feature = "nexa")]
fn index_scriptsig(tx: &Transaction) -> impl '_ + ParallelIterator<Item = Row> {
    tx.output
        .par_iter()
        .enumerate()
        .map(move |(index, output)| {
            {
                ScriptHashIndexRow::new(
                    &ScriptHash::normalized_from_txout(output),
                    &compute_output_hash(&tx.txidem(), index as u32),
                    if output.has_token() {
                        OutputFlags::HasTokens
                    } else {
                        OutputFlags::None
                    },
                )
            }
            .to_row()
        })
}
#[cfg(not(feature = "nexa"))]
fn index_scriptsig(tx: &Transaction) -> impl '_ + ParallelIterator<Item = Row> {
    tx.output
        .par_iter()
        .enumerate()
        .map(move |(index, output)| {
            {
                ScriptHashIndexRow::new(
                    &ScriptHash::from_script(&output.script_pubkey),
                    &compute_output_hash(&tx.txid(), index as u32),
                    OutputFlags::None,
                )
            }
            .to_row()
        })
}

#[cfg(feature = "nexa")]
fn index_token_outputs(tx: &Transaction) -> impl '_ + ParallelIterator<Item = Row> {
    use crate::{
        indexes::{outputtokenindex::OutputTokenIndexRow, tokenoutputindex::TokenOutputIndexRow},
        nexa::token::parse_token_from_scriptpubkey,
    };
    tx.output
        .par_iter()
        .enumerate()
        .filter_map(move |(i, out)| {
            if !out.has_token() {
                return None;
            }
            parse_token_from_scriptpubkey(&out.script_pubkey).map(|(token, amount)| {
                let outpoint_hash = &compute_output_hash(&tx.txidem(), i as u32);
                let out_token_row =
                    OutputTokenIndexRow::new(outpoint_hash, token.clone(), amount).to_row();

                let token_out_row = TokenOutputIndexRow::new(token, outpoint_hash).to_row();
                rayon::iter::once(out_token_row).chain(rayon::iter::once(token_out_row))
            })
        })
        .flatten()
}

#[cfg(not(feature = "nexa"))]
fn index_token_outputs(_tx: &Transaction) -> impl '_ + ParallelIterator<Item = Row> {
    const EMPTY: Vec<Row> = Vec::new();
    EMPTY.into_par_iter()
}

pub fn index_transaction<'a>(
    txn: &'a Transaction,
    height: usize,
    cashaccount: &'a Option<CashAccountParser>,
) -> impl 'a + ParallelIterator<Item = Row> {
    let txid = txn.txid();

    let inputs = txn
        .input
        .par_iter()
        .enumerate()
        .filter_map(move |(index, input)| {
            if is_coinbase_input(input) {
                None
            } else {
                let outputhash = get_output_hash(input);
                Some(InputIndexRow::new(outputhash, txid, index).to_row())
            }
        });
    let outputs = index_outputs(txn);

    let scriptsigs = index_scriptsig(txn);

    let cashaccount_row = match cashaccount {
        Some(cashaccount) => cashaccount.index_cashaccount(txn, height as u32),
        None => None,
    };

    let token_outputs = index_token_outputs(txn);

    // Persist transaction ID and confirmed height
    inputs
        .chain(outputs)
        .chain(scriptsigs)
        .chain(rayon::iter::once(
            HeightIndexRow::new(&txid, height as u32).to_row(),
        ))
        .chain(cashaccount_row)
        .chain(token_outputs)
}

pub(crate) fn index_block<'a>(
    block: &'a Block,
    height: usize,
    cashaccount: &'a Option<CashAccountParser>,
) -> impl 'a + ParallelIterator<Item = Row> {
    let blockhash = block.block_hash();
    let header_row = HeaderRow::new(&block.header).to_row();

    block
        .txdata
        .par_iter()
        .flat_map(move |txn| index_transaction(txn, height, cashaccount))
        .chain(rayon::iter::once(header_row))
        .chain(rayon::iter::once(last_indexed_block(&blockhash)))
}

pub(crate) fn last_indexed_block(blockhash: &BlockHash) -> Row {
    // Store last indexed block (i.e. all previous blocks were indexed)
    Row {
        key: b"L".to_vec().into_boxed_slice(),
        value: serialize(blockhash).into_boxed_slice(),
    }
}

fn read_indexed_headers(store: &DBStore) -> Vec<BlockHeader> {
    let (latest_blockhash, headers) = match store.get(b"L") {
        // latest blockheader persisted in the DB.
        Some(row) => {
            let tip: BlockHash = deserialize(&row).unwrap();
            let headers =
                HeaderRow::headers_up_to_tip(store, &tip).expect("Headers missing from database");
            (tip, headers)
        }
        None => (BlockHash::all_zeros(), vec![]),
    };
    info!("Latest indexed blockhash: {}", latest_blockhash.to_hex());

    assert_eq!(
        headers
            .first()
            .map(|h| h.prev_blockhash)
            .unwrap_or_else(BlockHash::all_zeros),
        BlockHash::all_zeros()
    );
    assert_eq!(
        headers
            .last()
            .map(BlockHeader::block_hash)
            .unwrap_or_else(BlockHash::all_zeros),
        latest_blockhash
    );
    headers
}

pub struct Index {
    store: DBStore,
    daemon: Arc<Daemon>,
    stats: Stats,
    batch_size: usize,
    chain: Chain,
    cashaccount_activation_height: u32,
}

impl Index {
    pub fn load(
        store: DBStore,
        mut chain: Chain,
        daemon: Arc<Daemon>,
        metrics: &Metrics,
        batch_size: usize,
        reindex_last_blocks: usize,
        cashaccount_activation_height: u32,
    ) -> Result<Self> {
        let headers = read_indexed_headers(&store);
        if !headers.is_empty() {
            let tip = headers.last().unwrap().block_hash();
            chain.load(headers, tip);
            chain.drop_last_headers(reindex_last_blocks as u64);
        }

        let stats = Stats::new(metrics);
        stats.observe_chain(&chain);
        Ok(Index {
            store,
            daemon,
            stats,
            batch_size,
            chain,
            cashaccount_activation_height,
        })
    }

    pub(crate) fn chain(&self) -> &Chain {
        &self.chain
    }

    pub fn update(&self, waiter: &Waiter) -> Result<(Vec<BlockHeader>, BlockHeader)> {
        let tip = self.daemon.getbestblockhash()?;
        let tip_height = self.daemon.getblockheader(&tip)?.1;
        let new_headers =
            metrics::observe_duration(&self.stats.update_duration, "new_headers", || {
                self.daemon.get_new_headers(&self.chain, &tip)
            })?;
        self.chain.update(
            new_headers
                .iter()
                .map(|(h, height)| NewHeader::from_header_and_height(h.clone(), *height))
                .collect(),
            Some(tip_height),
        );

        let chan = SyncChannel::new(self.batch_size);
        let sender = chan.sender();
        let blockhashes: Vec<BlockHash> = new_headers.iter().map(|(h, _)| h.block_hash()).collect();
        let daemon = self.daemon.clone();
        let fetcher = spawn("fetcher", move || {
            for blockhash in blockhashes.iter() {
                sender
                    .send(Some(daemon.getblock(blockhash)))
                    .context("failed sending blocks to be indexed")?;
            }
            sender
                .send(None)
                .context("failed sending explicit end of stream")?;
            Ok(())
        });
        let cashaccount = if self.cashaccount_activation_height > 0 {
            Some(CashAccountParser::new(Some(
                self.cashaccount_activation_height,
            )))
        } else {
            None
        };

        loop {
            waiter.poll()?;
            let timer = self.stats.start_timer("fetch");
            let block = chan
                .receiver()
                .recv()
                .context("block fetch exited prematurely")?;
            timer.observe_duration();
            if block.is_none() {
                break;
            }
            let block = block.unwrap();
            let block = block?;

            let blockhash = block.block_hash();
            let height = self
                .chain
                .get_block_height(&blockhash)
                .unwrap_or_else(|| panic!("missing header for block {}", blockhash));

            let timer = self.stats.start_timer("index+write");
            let indexed = index_block(&block, height as usize, &cashaccount);
            self.store.write(indexed, false);
            timer.observe_duration();
            self.stats.update(&block, height as usize);
        }

        let timer = self.stats.start_timer("flush");
        self.store.flush(); // make sure no row is left behind
        timer.observe_duration();

        fetcher.join().expect("block fetcher failed");
        let tip_header = self.chain.tip();
        assert_eq!(&tip, &tip_header.block_hash());
        self.stats.observe_chain(&self.chain);
        Ok((
            new_headers.into_iter().map(|(h, _)| h).collect(),
            tip_header,
        ))
    }

    pub fn read_store(&self) -> &dyn store::ReadStore {
        &self.store
    }
}
