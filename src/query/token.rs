use anyhow::Result;
use bitcoincash::Txid;
use rayon::prelude::*;

use crate::{
    chaindef::{TokenID, Transaction},
    encode::output_hash,
    indexes::{
        heightindex::HeightIndexRow, outputindex::OutputIndexRow,
        outputtokenindex::OutputTokenIndexRow, tokenoutputindex::TokenOutputIndexRow,
    },
    store::ReadStore,
    timeout::TimeoutTrigger,
};

use super::{queryutil::get_row, tx::TxQuery};

/**
 * Returns full transaction history for a token
 */
pub(crate) fn get_token_history(
    token_id: &TokenID,
    store: &dyn ReadStore,
    timeout: &TimeoutTrigger,
) -> Result<Vec<(Txid, u32)>> {
    let to_outpoint_filter = TokenOutputIndexRow::filter_by_token_id(token_id);
    let h = store
        .scan(&to_outpoint_filter)
        .par_iter()
        .map(TokenOutputIndexRow::from_row)
        .map(|r| {
            timeout.check()?;
            let txid_key = OutputIndexRow::filter_by_outpointhash_raw(*r.outpoint_hash_raw());
            let txid = get_row::<OutputIndexRow>(store, txid_key)
                .expect("txid not found for token output")
                .txid();
            let height_key = HeightIndexRow::filter_by_txid(&txid);
            let height = get_row::<HeightIndexRow>(store, height_key)
                .expect("height not found for txid")
                .height();
            Ok((txid, height))
        })
        .collect::<Result<Vec<(Txid, u32)>>>();

    let mut h = h?;

    // Sort by inverse height first, then txid.
    h.sort_unstable_by(|a, b| match b.1.cmp(&a.1) {
        std::cmp::Ordering::Less => std::cmp::Ordering::Less,
        std::cmp::Ordering::Equal => b.0.cmp(&a.0),
        std::cmp::Ordering::Greater => std::cmp::Ordering::Greater,
    });
    h.dedup();
    Ok(h)
}

/// Locates and returns the transaction that created a token.
pub(crate) fn find_token_genesis(
    token_id: &TokenID,
    store: &dyn ReadStore,
    txquery: &TxQuery,
) -> Result<(Transaction, u32)> {
    let to_outpoint_filter = TokenOutputIndexRow::filter_by_token_id(token_id);

    let lowest_height_txs: Vec<(Txid, u32)> = store
        .scan(&to_outpoint_filter)
        .iter()
        .map(TokenOutputIndexRow::from_row)
        .map(|r| {
            let txid_key = OutputIndexRow::filter_by_outpointhash_raw(*r.outpoint_hash_raw());
            let txid = get_row::<OutputIndexRow>(store, txid_key)
                .expect("txid not found for token output")
                .txid();
            let height_key = HeightIndexRow::filter_by_txid(&txid);
            let height = get_row::<HeightIndexRow>(store, height_key)
                .expect("height not found for txid")
                .height();
            (txid, height)
        })
        .fold(Vec::new(), |lowest, tx| {
            if lowest.is_empty() {
                vec![tx]
            } else {
                match tx.1.cmp(&lowest[0].1) {
                    std::cmp::Ordering::Less => vec![tx],
                    std::cmp::Ordering::Equal => {
                        lowest.into_iter().chain(std::iter::once(tx)).collect()
                    }
                    std::cmp::Ordering::Greater => lowest,
                }
            }
        });

    // One of the txs at lowest block height must be genesis, but we need to check which one.
    for (txid, height) in lowest_height_txs {
        // Mempool txs are expected to use MEMPOOL_HEIGHT
        assert!(height > 0);

        let tx = txquery.get(&txid, None, Some(height))?;

        // If any of the inputs contain token, then this tx is not the genesis.
        if tx.input.iter().any(|i| {
            let outpoint_hash = output_hash(&i.previous_output);
            let filter = OutputTokenIndexRow::filter_by_outpointhash_and_token(
                &outpoint_hash,
                token_id.clone(),
            );
            !store.scan(&filter).is_empty()
        }) {
            continue;
        } else {
            // No inputs had token, this tx must be genesis.
            return Ok((tx, height));
        }
    }
    bail!("Token {} not found", token_id.to_hex())
}
