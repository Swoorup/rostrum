use std::convert::TryInto;

use crate::chaindef::OutPointHash;
use crate::chaindef::ScriptHash;
use crate::chaindef::TokenID;
use crate::chaindef::Transaction;
use crate::indexes::heightindex::HeightIndexRow;
use crate::indexes::inputindex::InputIndexRow;
use crate::indexes::outputindex::OutputIndexRow;
use crate::indexes::outputtokenindex::OutputTokenIndexRow;
use crate::indexes::scripthashindex::OutputFlags;
use crate::indexes::scripthashindex::ScriptHashIndexRow;
use crate::indexes::DBRow;
use crate::mempool::{ConfirmationState, Tracker, MEMPOOL_HEIGHT};
use crate::query::primitives::{FundingOutput, SpendingInput};
use crate::query::tx::TxQuery;
use crate::store::{ReadStore, Row};
use crate::timeout::TimeoutTrigger;
use anyhow::Result;
use bitcoincash::hash_types::Txid;
use rayon::prelude::*;

pub(crate) fn get_row<T: DBRow>(store: &dyn ReadStore, key: Vec<u8>) -> Option<T> {
    let row = store.get(&key).map(|value| Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    });
    row.map(|r| T::from_row(&r))
}

/**
 * Fetche a single row found by key prefix.
 */
pub(crate) fn get_row_by_key_prefix<T: DBRow>(store: &dyn ReadStore, key: &[u8]) -> Option<T> {
    store.scan(key).first().map(|r| T::from_row(r))
}

pub fn height_by_txid(store: &dyn ReadStore, txid: &Txid) -> Option<u32> {
    let key = HeightIndexRow::filter_by_txid(txid);
    let value = store.get(&key)?;
    let height = HeightIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    })
    .height();
    if height == MEMPOOL_HEIGHT {
        None
    } else {
        Some(height)
    }
}

/**
 * Collect all utxos sent to a scripthash.
 */
pub fn outputs_by_scripthash(store: &dyn ReadStore, scripthash: ScriptHash) -> Vec<OutputIndexRow> {
    let filter = ScriptHashIndexRow::filter_by_scripthash(scripthash.to_vec().try_into().unwrap());

    let scripthash_entries = store.scan(&filter).len();
    let utxos: Vec<OutputIndexRow> = store
        .scan(&filter)
        .par_iter()
        .flat_map(|scripthash: &Row| -> Vec<OutputIndexRow> {
            let scripthash = ScriptHashIndexRow::from_row(scripthash);
            store
                .scan(&OutputIndexRow::filter_by_outpointhash(
                    &scripthash.outpointhash(),
                ))
                .iter()
                .map(OutputIndexRow::from_row)
                .collect()
        })
        .collect();
    trace!(
        "Found {} utxos, scripthash entries {}",
        utxos.len(),
        scripthash_entries,
    );
    utxos
}

pub fn get_txheight_by_id(store: &dyn ReadStore, txid: &Txid) -> Option<HeightIndexRow> {
    let key = HeightIndexRow::filter_by_txid(txid);
    let value = store.get(&key)?;
    Some(HeightIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    }))
}

/**
 * If token_id is provided, filters result by token ID.
 */
pub fn txheight_by_scripthash(
    store: &dyn ReadStore,
    scripthash: ScriptHash,
) -> Vec<HeightIndexRow> {
    outputs_by_scripthash(store, scripthash)
        .par_iter()
        .map(|output| output.txid())
        .map(|txid| {
            get_txheight_by_id(store, &txid)
                .unwrap_or_else(|| panic!("expected tx {} to have a height", txid))
        })
        .collect()
}

fn get_output_rows(
    timeout: &TimeoutTrigger,
    store: &dyn ReadStore,
    outpointhash: [u8; 32],
) -> Result<Vec<OutputIndexRow>> {
    store
        .scan(&OutputIndexRow::filter_by_outpointhash_raw(outpointhash))
        .par_iter()
        .map(|row| {
            timeout.check()?;
            Ok(OutputIndexRow::from_row(row))
        })
        .collect()
}

pub fn token_from_outpoint(
    store: &dyn ReadStore,
    outputhash_raw: &[u8; 32],
) -> Option<(TokenID, i64)> {
    let key = OutputTokenIndexRow::filter_by_outpointhash_raw(outputhash_raw);
    match get_row_by_key_prefix::<OutputTokenIndexRow>(store, &key) {
        Some(token_row) => {
            let amount = token_row.token_amount();
            Some((token_row.into_token_id(), amount))
        }
        None => None,
    }
}

pub fn scripthash_to_fundingoutput(
    store: &dyn ReadStore,
    scripthash: ScriptHash,
    mempool: Option<&Tracker>,
    output_flags: OutputFlags,
    timeout: &TimeoutTrigger,
) -> Result<Vec<FundingOutput>> {
    let scripthash_filter = match output_flags {
        OutputFlags::None => {
            ScriptHashIndexRow::filter_by_scripthash(scripthash.to_vec().try_into().unwrap())
        }
        OutputFlags::HasTokens => ScriptHashIndexRow::filter_by_scripthash_with_token(
            scripthash.to_vec().try_into().unwrap(),
        ),
    };

    let outpoints: Vec<OutputIndexRow> = store
        .scan(&scripthash_filter)
        .par_iter()
        .map(ScriptHashIndexRow::from_row)
        .flat_map(|scripthash| -> Result<Vec<OutputIndexRow>> {
            get_output_rows(timeout, store, scripthash.outpointhash_raw())
        })
        .flatten()
        .collect();

    Ok(outpoints
        .par_iter()
        .map(|out| {
            let txid = out.txid();
            let height = height_by_txid(store, &txid);
            let token = if output_flags == OutputFlags::HasTokens {
                token_from_outpoint(store, out.hash_raw())
            } else {
                None
            };
            FundingOutput {
                txid,
                vout: out.index(),
                outpoint_hash: out.hash(),
                height,
                value: out.value(),
                state: confirmation_state(mempool, &txid, height),
                token_amount: if let Some((_, a)) = token {
                    Some(a)
                } else {
                    None
                },
                token_id: if let Some((t, _)) = token {
                    Some(t)
                } else {
                    None
                },
            }
        })
        .collect())
}

fn confirmation_state(
    mempool: Option<&Tracker>,
    txid: &Txid,
    height: Option<u32>,
) -> ConfirmationState {
    // If mempool parameter is not passed, this implies that it is known
    // that the transaction is confirmed.
    if mempool.is_none() || height.is_some() && height != Some(MEMPOOL_HEIGHT) {
        return ConfirmationState::Confirmed;
    }
    let mempool = mempool.unwrap();
    mempool.tx_confirmation_state(txid, height)
}

pub fn find_spending_input(
    store: &dyn ReadStore,
    funding: &FundingOutput,
    mempool: Option<&Tracker>,
) -> Option<SpendingInput> {
    let spender = tx_spending_outpoint(store, &funding.outpoint_hash)?;

    let txid = spender.txid();
    let height = height_by_txid(store, &txid);

    Some(SpendingInput {
        txn_id: txid,
        height: height.unwrap_or_default(),
        funding_output_hash: funding.outpoint_hash,
        value: funding.value,
        state: confirmation_state(mempool, &txid, height),
    })
}

pub fn get_tx_spending_prevout(
    store: &dyn ReadStore,
    txquery: &TxQuery,
    outpoint: &OutPointHash,
) -> Result<
    Option<(
        Transaction,
        u32, /* input index */
        u32, /* confirmation height */
    )>,
> {
    let spender = tx_spending_outpoint(store, outpoint);
    if spender.is_none() {
        return Ok(None);
    };
    let spender = spender.unwrap();
    let txid = spender.txid();
    let height = height_by_txid(store, &txid);
    let tx = txquery.get(&txid, None, height)?;
    Ok(Some((
        tx,
        spender.index() as u32,
        height.unwrap_or_default(),
    )))
}

pub fn tx_spending_outpoint(
    store: &dyn ReadStore,
    outpoint: &OutPointHash,
) -> Option<InputIndexRow> {
    let key = InputIndexRow::filter_by_outputhash(outpoint);
    let value = store.get(&key)?;
    Some(InputIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    }))
}

pub fn get_tx_funding_prevout(
    store: &dyn ReadStore,
    txquery: &TxQuery,
    outpoint: &OutPointHash,
) -> Result<
    Option<(
        Transaction,
        u32, /* input index */
        u32, /* confirmation height */
    )>,
> {
    let spender = tx_funding_outpoint(store, outpoint);
    if spender.is_none() {
        return Ok(None);
    };
    let spender = spender.unwrap();
    let txid = spender.txid();
    let height = height_by_txid(store, &txid);
    let tx = txquery.get(&txid, None, height)?;
    Ok(Some((
        tx,
        spender.index() as u32,
        height.unwrap_or_default(),
    )))
}

/**
 * Locate transaction that funded given output (created this utxo)
 */
pub fn tx_funding_outpoint(
    store: &dyn ReadStore,
    outpoint: &OutPointHash,
) -> Option<OutputIndexRow> {
    let key = OutputIndexRow::filter_by_outpointhash(outpoint);
    let value = store.get(&key)?;
    Some(OutputIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value.into_boxed_slice(),
    }))
}
