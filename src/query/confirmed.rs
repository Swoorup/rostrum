use crate::chaindef::OutPointHash;
use crate::chaindef::ScriptHash;
use crate::chaindef::Transaction;
use crate::indexes::scripthashindex::OutputFlags;
use crate::query::primitives::{FundingOutput, SpendingInput};
use crate::query::queryutil::{find_spending_input, get_tx_spending_prevout};
use crate::query::tx::TxQuery;
use crate::store::ReadStore;
use crate::timeout::TimeoutTrigger;
use anyhow::Result;
use rayon::prelude::*;
use std::sync::Arc;

use super::queryutil::scripthash_to_fundingoutput;

pub struct ConfirmedQuery {
    txquery: Arc<TxQuery>,
    duration: Arc<prometheus::HistogramVec>,
}

impl ConfirmedQuery {
    pub fn new(txquery: Arc<TxQuery>, duration: Arc<prometheus::HistogramVec>) -> ConfirmedQuery {
        ConfirmedQuery { txquery, duration }
    }

    /// Query for confirmed outputs that funding scripthash.
    pub fn get_funding(
        &self,
        read_store: &dyn ReadStore,
        scripthash: ScriptHash,
        output_flags: OutputFlags,
        timeout: &TimeoutTrigger,
    ) -> Result<Vec<FundingOutput>> {
        let timer = self
            .duration
            .with_label_values(&["confirmed_status_funding"])
            .start_timer();

        let funding =
            scripthash_to_fundingoutput(read_store, scripthash, None, output_flags, timeout);
        timer.observe_duration();
        funding
    }

    /// Query for confirmed inputs that have been spent from scripthash.
    ///
    /// This requires list of transactions funding the scripthash, obtained
    /// with get_funding.
    pub fn get_spending(
        &self,
        read_store: &dyn ReadStore,
        confirmed_funding: &[FundingOutput],
    ) -> Vec<SpendingInput> {
        let timer = self
            .duration
            .with_label_values(&["confirmed_status_spending"])
            .start_timer();

        let spending = confirmed_funding
            .par_iter()
            .filter_map(|out| find_spending_input(read_store, out, None))
            .collect();

        timer.observe_duration();
        spending
    }

    pub fn get_tx_spending_prevout(
        &self,
        read_store: &dyn ReadStore,
        outpoint: &OutPointHash,
    ) -> Result<
        Option<(
            Transaction,
            u32, /* input index */
            u32, /* confirmation height */
        )>,
    > {
        get_tx_spending_prevout(read_store, &*self.txquery, outpoint)
    }
}
