use crate::chaindef::OutPointHash;
use crate::chaindef::ScriptHash;
use crate::chaindef::Transaction;
use crate::indexes::scripthashindex::OutputFlags;
use crate::mempool::Tracker;
use crate::query::primitives::{FundingOutput, SpendingInput};
use crate::query::queryutil::{find_spending_input, get_tx_spending_prevout};
use crate::query::tx::TxQuery;
use crate::timeout::TimeoutTrigger;
use anyhow::Result;
use bitcoincash::hash_types::Txid;
use rayon::prelude::*;
use std::collections::HashMap;
use std::sync::Arc;

use crate::query::queryutil::scripthash_to_fundingoutput;

pub struct UnconfirmedQuery {
    txquery: Arc<TxQuery>,
    duration: Arc<prometheus::HistogramVec>,
}

impl UnconfirmedQuery {
    pub fn new(txquery: Arc<TxQuery>, duration: Arc<prometheus::HistogramVec>) -> UnconfirmedQuery {
        UnconfirmedQuery { txquery, duration }
    }

    pub fn get_funding(
        &self,
        tracker: &Tracker,
        scripthash: ScriptHash,
        output_flags: OutputFlags,
        timeout: &TimeoutTrigger,
    ) -> Result<Vec<FundingOutput>> {
        let timer = self
            .duration
            .with_label_values(&["mempool_status_funding"])
            .start_timer();
        let funding = scripthash_to_fundingoutput(
            tracker.index(),
            scripthash,
            Some(tracker),
            output_flags,
            timeout,
        );
        timer.observe_duration();
        funding
    }

    /// Get unconfirmed use of input spending from scripthash destination.
    ///
    /// unconfirmed_funding is obtain by calling self.get_funding,
    /// confirmed_funding is obtained by calling ConfirmedQuery::get_funding
    pub fn get_spending(
        &self,
        tracker: &Tracker,
        unconfirmed_funding: &[FundingOutput],
        confirmed_funding: &[FundingOutput],
        timeout: &TimeoutTrigger,
    ) -> Result<Vec<SpendingInput>> {
        let timer = self
            .duration
            .with_label_values(&["mempool_status_spending"])
            .start_timer();

        let funding_iter = unconfirmed_funding
            .par_iter()
            .chain(confirmed_funding.par_iter());

        let spending: Vec<Option<SpendingInput>> = funding_iter
            .map(|funding_output| {
                timeout.check()?;
                Ok(find_spending_input(
                    tracker.index(),
                    funding_output,
                    Some(tracker),
                ))
            })
            .collect::<Result<Vec<Option<SpendingInput>>>>()?;

        timer.observe_duration();
        Ok(spending.into_iter().flatten().collect())
    }

    /// Calculate fees for unconfirmed mempool transactions.
    pub fn get_tx_fees(
        &self,
        tracker: &Tracker,
        funding: &[FundingOutput],
        spending: &[SpendingInput],
    ) -> HashMap<Txid, u64> {
        let mut txn_fees = HashMap::new();
        for mempool_txid in funding
            .iter()
            .map(|f| f.txid)
            .chain(spending.iter().map(|s| s.txn_id))
        {
            tracker
                .get_fee(&mempool_txid)
                .map(|fee| txn_fees.insert(mempool_txid, fee));
        }
        txn_fees
    }

    pub fn get_tx_spending_prevout(
        &self,
        tracker: &Tracker,
        outpoint: &OutPointHash,
    ) -> Result<
        Option<(
            Transaction,
            u32, /* input index */
            u32, /* confirmation height */
        )>,
    > {
        get_tx_spending_prevout(tracker.index(), &*self.txquery, outpoint)
    }
}
