use crate::chaindef::{OutPointHash, TokenID};
use crate::mempool::ConfirmationState;
use bitcoincash::hash_types::Txid;

fn ser_height<S>(h: &Option<u32>, s: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    if let Some(h) = h {
        s.serialize_u32(*h)
    } else {
        s.serialize_u32(0)
    }
}

#[derive(Serialize)]
pub struct FundingOutput {
    #[serde(rename = "tx_hash")]
    pub txid: Txid,
    #[serde(rename = "tx_pos")]
    pub vout: u32,
    pub outpoint_hash: OutPointHash,
    #[serde(serialize_with = "ser_height")]
    pub height: Option<u32>,
    pub value: u64,
    #[serde(skip)]
    pub state: ConfirmationState,
    #[serde(skip_serializing_if = "Option::is_none", rename = "token_id_hex")]
    pub token_id: Option<TokenID>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub token_amount: Option<i64>,
}

pub struct SpendingInput {
    pub txn_id: Txid,
    pub height: u32,
    pub funding_output_hash: OutPointHash,
    pub value: u64,
    pub state: ConfirmationState,
}
