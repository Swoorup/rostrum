use crate::chaindef::BlockHeader;
use crate::chaindef::OutPointHash;
use crate::chaindef::TokenID;
use crate::indexes::scripthashindex::OutputFlags;
use crate::store::ReadStore;
use rayon::prelude::*;

use crate::chaindef::ScriptHash;
use crate::chaindef::Transaction;
use crate::metrics;
use bitcoincash::consensus::encode::serialize;
use bitcoincash::hash_types::{TxMerkleNode, Txid};
use bitcoincash::hashes::hex::ToHex;
use bitcoincash::hashes::sha256d::Hash as Sha256dHash;
use bitcoincash::hashes::Hash;
use bitcoincash::network::constants::Network;
use serde_json::Value;
use sha2::{Digest, Sha256};
use std::collections::{HashMap, HashSet};
use std::convert::TryInto;
use std::ops::Add;
use std::ops::Sub;
use std::sync::{Arc, RwLock};

use crate::app::App;
use crate::cache::TransactionCache;
use crate::cashaccount::{txids_by_cashaccount, CashAccountParser};
use crate::chaindef::BlockHash;
use crate::mempool::{ConfirmationState, Tracker};
use crate::metrics::Metrics;
#[cfg(feature = "nexa")]
use crate::nexa::hash_types::TxIdem;
use crate::query::confirmed::ConfirmedQuery;
use crate::query::header::HeaderQuery;
use crate::query::primitives::{FundingOutput, SpendingInput};
use crate::query::queryutil::txheight_by_scripthash;
use crate::query::tx::TxQuery;
use crate::query::unconfirmed::UnconfirmedQuery;
use crate::timeout::TimeoutTrigger;
use anyhow::{Context, Result};

use self::queryutil::get_tx_funding_prevout;
use self::queryutil::token_from_outpoint;

pub mod confirmed;
pub mod header;
pub mod primitives;
pub mod queryutil;
pub mod token;
pub mod tx;
pub mod unconfirmed;

pub struct Status {
    confirmed: (Vec<FundingOutput>, Vec<SpendingInput>),
    mempool: (Vec<FundingOutput>, Vec<SpendingInput>),
    txn_fees: HashMap<Txid, u64>,
}

fn calc_balance((funding, spending): &(Vec<FundingOutput>, Vec<SpendingInput>)) -> i64 {
    let funded: u64 = funding.par_iter().map(|output| output.value).sum();
    let spent: u64 = spending.par_iter().map(|input| input.value).sum();
    funded as i64 - spent as i64
}

/// When looking up mempool balance, the index for confirmed transactions
/// needs to be provided. Other than in this case, all entires are available
/// in funding list.
fn calc_token_balance(
    index: Option<&dyn ReadStore>,
    (funding, spending): &(Vec<FundingOutput>, Vec<SpendingInput>),
) -> HashMap<TokenID, i64> {
    let funders: HashMap<&OutPointHash, &FundingOutput> = funding
        .par_iter()
        .filter_map(|f| {
            if f.token_id.is_some() {
                Some((&f.outpoint_hash, f))
            } else {
                None
            }
        })
        .collect();

    let mut balance: HashMap<TokenID, i64> = HashMap::new();
    for s in spending {
        let (token_id, token_amount) = if let Some(f) = funders.get(&s.funding_output_hash) {
            (
                f.token_id.as_ref().unwrap().clone(),
                f.token_amount.unwrap(),
            )
        } else if let Some(i) = index {
            if let Some(t) =
                token_from_outpoint(i, &s.funding_output_hash.to_vec().try_into().unwrap())
            {
                t
            } else {
                continue;
            }
        } else {
            continue;
        };
        if let Some(amount) = balance.get_mut(&token_id) {
            *amount = amount.sub(token_amount);
        } else {
            balance.insert(token_id, -token_amount);
        }
    }

    for f in funders.values() {
        if let Some(amount) = balance.get_mut(f.token_id.as_ref().unwrap()) {
            *amount = amount.add(f.token_amount.unwrap());
        } else {
            balance.insert(
                f.token_id.as_ref().unwrap().clone(),
                f.token_amount.unwrap(),
            );
        }
    }
    balance
}

#[derive(Serialize)]
pub struct HistoryItem {
    pub tx_hash: Txid,
    pub height: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fee: Option<u64>, // need to be set only for unconfirmed transactions (i.e. height <= 0)
}

impl Status {
    fn funding(&self) -> impl ParallelIterator<Item = &FundingOutput> {
        self.confirmed.0.par_iter().chain(self.mempool.0.par_iter())
    }

    fn spending(&self) -> impl ParallelIterator<Item = &SpendingInput> {
        self.confirmed.1.par_iter().chain(self.mempool.1.par_iter())
    }

    pub fn confirmed_balance(&self) -> i64 {
        calc_balance(&self.confirmed)
    }

    pub fn mempool_balance(&self) -> i64 {
        calc_balance(&self.mempool)
    }

    pub fn confirmed_token_balance(&self) -> HashMap<TokenID, i64> {
        calc_token_balance(None, &self.confirmed)
    }

    pub fn mempool_token_balance(&self, index: &dyn ReadStore) -> HashMap<TokenID, i64> {
        calc_token_balance(Some(index), &self.mempool)
    }

    pub fn history(&self, filter_token: Option<TokenID>) -> Vec<HistoryItem> {
        let mut items: Vec<HistoryItem> = self
            .funding()
            .filter_map(|f| {
                if filter_token.is_some() && f.token_id != filter_token {
                    return None;
                }
                let height: i32 = match f.state {
                    ConfirmationState::Confirmed => f.height.unwrap() as i32,
                    ConfirmationState::InMempool => 0,
                    ConfirmationState::UnconfirmedParent => -1,
                    ConfirmationState::Indeterminate => {
                        debug_assert!(false, "ConfirmationState cannot be Indeterminate");
                        0
                    }
                };
                Some((f.txid, height, f.token_id.as_ref(), f.token_amount))
            })
            .chain(self.spending().map(|s| {
                let height: i32 = match s.state {
                    ConfirmationState::Confirmed => s.height as i32,
                    ConfirmationState::InMempool => 0,
                    ConfirmationState::UnconfirmedParent => -1,
                    ConfirmationState::Indeterminate => {
                        debug_assert!(false, "ConfirmationState cannot be Indeterminate");
                        0
                    }
                };
                (s.txn_id, height, None, None)
            }))
            .map(|item| HistoryItem {
                height: item.1,
                tx_hash: item.0,
                fee: self.txn_fees.get(&item.0).cloned(),
            })
            .collect();

        items.par_sort_unstable_by(|a, b| {
            if a.height == b.height {
                // Order by little endian tx hash if height is the same,
                // in most cases, this order is the same as on the blockchain.
                return b.tx_hash.cmp(&a.tx_hash);
            }
            if a.height > 0 && b.height > 0 {
                return a.height.cmp(&b.height);
            }

            // mempool txs should be sorted last, so add to it a large number
            let mut a_height = a.height;
            let mut b_height = b.height;
            if a_height <= 0 {
                a_height = 0xEE_EEEE + a_height.abs();
            }
            if b_height <= 0 {
                b_height = 0xEE_EEEE + b_height.abs();
            }
            a_height.cmp(&b_height)
        });
        items.dedup_by_key(|x| x.tx_hash);
        items
    }

    pub fn unspent(&self, filter_token: Option<TokenID>) -> Vec<&FundingOutput> {
        let mut outputs_map: HashMap<&OutPointHash, &FundingOutput> =
            self.funding().map(|f| (&f.outpoint_hash, f)).collect();

        let spent: Vec<&OutPointHash> = self.spending().map(|s| &s.funding_output_hash).collect();
        for s in spent {
            if outputs_map.remove(&s).is_none() {
                warn!("failed to remove {:?}", s.to_hex());
            }
        }
        let mut outputs = outputs_map
            .into_iter()
            .map(|item| item.1)
            .filter(|f| {
                if let Some(t) = filter_token.as_ref() {
                    f.token_id
                        .as_ref()
                        .map(|output_token| output_token.eq(t))
                        .is_some()
                } else {
                    true
                }
            })
            .collect::<Vec<&FundingOutput>>();
        outputs.sort_unstable_by_key(|out| out.height);
        outputs
    }

    pub fn hash(&self, filter_token: Option<TokenID>) -> Option<[u8; 32]> {
        let txns = self.history(filter_token);
        if txns.is_empty() {
            None
        } else {
            let mut sha2 = Sha256::new();
            let parts: Vec<String> = txns
                .into_par_iter()
                .map(|t| format!("{}:{}:", t.tx_hash.to_hex(), t.height))
                .collect();
            for p in parts {
                sha2.update(p.as_bytes());
            }
            Some(sha2.finalize().into())
        }
    }
}

fn merklize<T: Hash>(left: T, right: T) -> T {
    let data = [&left[..], &right[..]].concat();
    <T as Hash>::hash(&data)
}

fn create_merkle_branch_and_root<T: Hash>(mut hashes: Vec<T>, mut index: usize) -> (Vec<T>, T) {
    let mut merkle = vec![];
    while hashes.len() > 1 {
        if hashes.len() % 2 != 0 {
            let last = *hashes.last().unwrap();
            hashes.push(last);
        }
        index = if index % 2 == 0 { index + 1 } else { index - 1 };
        merkle.push(hashes[index]);
        index /= 2;
        hashes = hashes
            .chunks(2)
            .map(|pair| merklize(pair[0], pair[1]))
            .collect()
    }
    (merkle, hashes[0])
}

pub struct Query {
    app: Arc<App>,
    tracker: Arc<RwLock<Tracker>>,
    duration: Arc<prometheus::HistogramVec>,
    confirmed: ConfirmedQuery,
    unconfirmed: UnconfirmedQuery,
    tx: Arc<TxQuery>,
    header: Arc<HeaderQuery>,
}

impl Query {
    pub fn new(
        app: Arc<App>,
        metrics: &Metrics,
        tx_cache: TransactionCache,
        network: Network,
    ) -> Result<Arc<Query>> {
        let daemon = app.daemon().reconnect()?;
        let duration = Arc::new(metrics.histogram_vec(
            "rostrum_query_duration",
            "Request duration (in seconds)",
            &["type"],
            metrics::default_duration_buckets(),
        ));
        let tracker = Arc::new(RwLock::new(Tracker::new(metrics)));
        let header = Arc::new(HeaderQuery::new(app.clone()));
        let tx = Arc::new(TxQuery::new(
            tx_cache,
            daemon,
            tracker.clone(),
            header.clone(),
            duration.clone(),
            network,
        ));
        let confirmed = ConfirmedQuery::new(tx.clone(), duration.clone());
        let unconfirmed = UnconfirmedQuery::new(tx.clone(), duration.clone());
        Ok(Arc::new(Query {
            app,
            tracker,
            duration,
            confirmed,
            unconfirmed,
            tx,
            header,
        }))
    }

    pub fn status_mempool(
        &self,
        scripthash: ScriptHash,
        output_flags: OutputFlags,
        timeout: &TimeoutTrigger,
    ) -> Result<Status> {
        let store = self.app.index().read_store();
        let confirmed_funding =
            self.confirmed
                .get_funding(store, scripthash, output_flags, timeout)?;

        let tracker = self.tracker.read().unwrap();
        let unconfirmed_funding =
            self.unconfirmed
                .get_funding(&tracker, scripthash, output_flags, timeout)?;

        let unconfirmed_spending = self.unconfirmed.get_spending(
            &tracker,
            &confirmed_funding,
            &unconfirmed_funding,
            timeout,
        )?;

        let txn_fees =
            self.unconfirmed
                .get_tx_fees(&tracker, &unconfirmed_funding, &unconfirmed_spending);
        let confirmed = (vec![], vec![]);
        let mempool = (unconfirmed_funding, unconfirmed_spending);

        Ok(Status {
            confirmed,
            mempool,
            txn_fees,
        })
    }

    pub fn status(
        &self,
        scripthash: ScriptHash,
        output_flags: OutputFlags,
        timeout: &TimeoutTrigger,
    ) -> Result<Status> {
        let store = self.app.index().read_store();
        let confirmed_funding =
            self.confirmed
                .get_funding(store, scripthash, output_flags, timeout)?;

        let confirmed_spending = self.confirmed.get_spending(store, &confirmed_funding);

        let tracker = self.tracker.read().unwrap();
        let unconfirmed_funding =
            self.unconfirmed
                .get_funding(&tracker, scripthash, output_flags, timeout)?;

        let unconfirmed_spending = self.unconfirmed.get_spending(
            &tracker,
            &confirmed_funding,
            &unconfirmed_funding,
            timeout,
        )?;

        let txn_fees =
            self.unconfirmed
                .get_tx_fees(&tracker, &unconfirmed_funding, &unconfirmed_spending);
        let confirmed = (confirmed_funding, confirmed_spending);
        let mempool = (unconfirmed_funding, unconfirmed_spending);

        Ok(Status {
            confirmed,
            mempool,
            txn_fees,
        })
    }

    #[cfg(feature = "nexa")]
    fn get_block_height(&self, header: &BlockHeader) -> Option<u64> {
        Some(header.height())
    }

    #[cfg(not(feature = "nexa"))]
    fn get_block_height(&self, header: &BlockHeader) -> Option<u64> {
        self.app
            .index()
            .chain()
            .get_block_height(&header.block_hash())
    }

    pub fn get_confirmed_blockhash(&self, tx_hash: &Txid) -> Result<Value> {
        let header = self.header.get_by_txid(tx_hash, None)?;
        if header.is_none() {
            bail!("tx {} is unconfirmed or does not exist", tx_hash);
        }
        let header = header.unwrap();
        let height = self
            .get_block_height(&header)
            .context(anyhow!("No height for block {}", header.block_hash()))?;
        Ok(json!({
            "block_hash": header.block_hash(),
            "block_height": height,
        }))
    }

    pub fn get_headers(&self, heights: &[usize]) -> Vec<BlockHeader> {
        let _timer = self
            .duration
            .with_label_values(&["get_headers"])
            .start_timer();
        let index = self.app.index();
        heights
            .iter()
            .filter_map(|height| index.chain().get_block_header(*height))
            .collect()
    }

    pub(crate) fn get_best_header(&self) -> BlockHeader {
        self.app.index().chain().tip()
    }

    pub fn getblocktxids(&self, blockhash: &BlockHash) -> Result<Vec<Txid>> {
        self.app.daemon().getblocktxids(blockhash)
    }

    pub fn get_merkle_proof(
        &self,
        tx_hash: &Txid,
        height: usize,
    ) -> Result<(Vec<TxMerkleNode>, usize)> {
        let header_entry = self
            .app
            .index()
            .chain()
            .get_block_header(height)
            .context(format!("missing block #{}", height))?;
        let txids = self
            .app
            .daemon()
            .getblocktxids(&header_entry.block_hash())?;
        let pos = txids
            .iter()
            .position(|txid| txid == tx_hash)
            .context(format!("missing txid {}", tx_hash))?;
        let tx_nodes: Vec<TxMerkleNode> = txids
            .into_iter()
            .map(|txid| TxMerkleNode::from_inner(txid.into_inner()))
            .collect();
        let (branch, _root) = create_merkle_branch_and_root(tx_nodes, pos);
        Ok((branch, pos))
    }

    pub fn get_header_merkle_proof(
        &self,
        height: usize,
        cp_height: usize,
    ) -> Result<(Vec<Sha256dHash>, Sha256dHash)> {
        if cp_height < height {
            bail!("cp_height #{} < height #{}", cp_height, height);
        }

        let best_height = self.app.index().chain().height();
        if best_height < (cp_height as u64) {
            bail!(
                "cp_height #{} above best block height #{}",
                cp_height,
                best_height
            );
        }

        let heights: Vec<usize> = (0..=cp_height).collect();
        let header_hashes: Vec<BlockHash> = self
            .get_headers(&heights)
            .into_iter()
            .map(|h| h.block_hash())
            .collect();
        let merkle_nodes: Vec<Sha256dHash> = header_hashes
            .iter()
            .map(|block_hash| Sha256dHash::from_inner(block_hash.into_inner()))
            .collect();
        assert_eq!(header_hashes.len(), heights.len());
        Ok(create_merkle_branch_and_root(merkle_nodes, height))
    }

    pub fn get_id_from_pos(
        &self,
        height: usize,
        tx_pos: usize,
        want_merkle: bool,
    ) -> Result<(Txid, Vec<TxMerkleNode>)> {
        let header_entry = self
            .app
            .index()
            .chain()
            .get_block_header(height)
            .context(format!("missing block #{}", height))?;

        let txids = self
            .app
            .daemon()
            .getblocktxids(&header_entry.block_hash())?;
        let txid = *txids.get(tx_pos).context(format!(
            "No tx in position #{} in block #{}",
            tx_pos, height
        ))?;

        let tx_nodes = txids
            .into_iter()
            .map(|txid| TxMerkleNode::from_inner(txid.into_inner()))
            .collect();

        let branch = if want_merkle {
            create_merkle_branch_and_root(tx_nodes, tx_pos).0
        } else {
            vec![]
        };
        Ok((txid, branch))
    }

    #[cfg(not(feature = "nexa"))]
    pub fn broadcast(&self, txn: &Transaction) -> Result<Txid> {
        self.app.daemon().broadcast(txn)
    }

    #[cfg(feature = "nexa")]
    pub fn broadcast(&self, txn: &Transaction) -> Result<TxIdem> {
        self.app.daemon().broadcast(txn)
    }

    pub fn update_mempool(&self) -> Result<HashSet<Txid>> {
        let _timer = self
            .duration
            .with_label_values(&["update_mempool"])
            .start_timer();
        self.tracker
            .write()
            .unwrap()
            .update(self.app.daemon(), self.tx())
    }

    /// Returns [vsize, fee_rate] pairs (measured in vbytes and satoshis).
    pub fn get_fee_histogram(&self) -> Vec<(f32, u32)> {
        self.tracker.read().unwrap().fee_histogram().clone()
    }

    // Fee rate [BTC/kB] to be confirmed in `blocks` from now.
    pub fn estimate_fee(&self, blocks: usize) -> f64 {
        let mut total_vsize = 0u32;
        let mut last_fee_rate = 0.0;
        let blocks_in_vbytes = (blocks * 1_000_000) as u32; // assume ~1MB blocks
        for (fee_rate, vsize) in self.tracker.read().unwrap().fee_histogram() {
            last_fee_rate = *fee_rate;
            total_vsize += vsize;
            if total_vsize >= blocks_in_vbytes {
                break; // under-estimate the fee rate a bit
            }
        }
        (last_fee_rate as f64) * 1e-5 // [BTC/kB] = 10^5 [sat/B]
    }

    pub fn get_banner(&self) -> Result<String> {
        self.app.get_banner()
    }

    pub fn get_cashaccount_txs(
        &self,
        name: &str,
        height: u32,
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let cashaccount_txs = txids_by_cashaccount(self.app.index().read_store(), name, height);

        #[derive(Serialize, Deserialize, Debug)]
        struct AccountTx {
            tx: String,
            height: u32,
            blockhash: String,
        }
        let header = self
            .header()
            .at_height(height as usize)
            .context(format!("missing header at height {}", height))?;
        let blockhash = header.block_hash();
        let blockhash_hex = blockhash.to_hex();

        let mut result: Vec<AccountTx> = vec![];
        let parser = CashAccountParser::new(None);

        for txid in cashaccount_txs {
            let tx = self.tx.get(&txid, Some(&blockhash), None)?;

            // Filter on name in case of txid prefix collision
            if !parser.has_cashaccount(&tx, name) {
                continue;
            }

            result.push({
                AccountTx {
                    tx: hex::encode(&serialize(&tx)),
                    height,
                    blockhash: blockhash_hex.clone(),
                }
            });
            timeout.check()?;
        }

        Ok(json!(result))
    }

    /// Find first outputs to scripthash
    pub fn scripthash_first_use(&self, scripthash: ScriptHash) -> Result<Option<(u32, Txid)>> {
        let get_tx = |store| {
            let txs = txheight_by_scripthash(store, scripthash);
            let tx = txs.iter().min_by(|a, b| a.height().cmp(&b.height()));

            if let Some(tx) = tx {
                Ok(Some((tx.height(), tx.txid())))
            } else {
                Ok(None)
            }
        };

        // Look at blockchain first
        if let Some(tx) = get_tx(self.app.index().read_store())? {
            return Ok(Some(tx));
        }

        // No match in the blockchain, try the mempool also.
        let tracker = self.tracker.read().unwrap();
        get_tx(tracker.index())
    }

    pub fn get_relayfee(&self) -> Result<f64> {
        self.app.daemon().get_relayfee()
    }

    pub fn tx(&self) -> &TxQuery {
        &self.tx
    }

    pub fn header(&self) -> &HeaderQuery {
        &self.header
    }

    pub fn get_tx_spending_prevout(
        &self,
        outpoint: &OutPointHash,
    ) -> Result<
        Option<(
            Transaction,
            u32, /* input index */
            u32, /* confirmation height */
        )>,
    > {
        {
            let tracker = self.tracker.read().unwrap();
            let spent = self
                .unconfirmed
                .get_tx_spending_prevout(&tracker, outpoint)?;
            if spent.is_some() {
                return Ok(spent);
            }
        }
        let store = self.app.index().read_store();
        self.confirmed.get_tx_spending_prevout(store, outpoint)
    }

    /**
     * Get the transaction funding a output (a utxo).
     *
     * Returns the transaction itself, the output index and height it was confirmed in (0 for unconfirmed).
     */
    pub fn get_tx_funding_prevout(
        &self,
        prevout: &OutPointHash,
    ) -> Result<
        Option<(
            Transaction,
            u32, /* output index */
            u32, /* confirmation height */
        )>,
    > {
        {
            let tracker = self.tracker.read().unwrap();
            let spent = get_tx_funding_prevout(tracker.index(), &self.tx, prevout)?;
            if spent.is_some() {
                return Ok(spent);
            }
        }

        get_tx_funding_prevout(self.app.index().read_store(), &self.tx, prevout)
    }

    pub fn confirmed_index(&self) -> &dyn ReadStore {
        self.app.index().read_store()
    }

    pub fn unconfirmed_index(&self) -> std::sync::RwLockReadGuard<'_, Tracker> {
        self.tracker.read().unwrap()
    }
}
