use crate::chaindef::ScriptHash;
use crate::nexa::cashaddr::decode as nexaddr_decode;
use crate::nexa::cashaddr::version_byte_flags;
use crate::nexa::transaction::TxOutType;
use anyhow::Result;
use bitcoincash::blockdata::opcodes;
use bitcoincash::blockdata::script::{Builder, Script};
use bitcoincash_addr::{Address, HashType};

pub fn decode_address(address: &str) -> Result<(Vec<u8>, u8)> {
    let bchaddr_decoded = Address::decode(address);
    if let Ok(decoded) = bchaddr_decoded {
        return match decoded.hash_type {
            HashType::Key => Ok((decoded.body, version_byte_flags::TYPE_P2PKH)),
            HashType::Script => Ok((decoded.body, version_byte_flags::TYPE_P2SH)),
        };
    };
    let nexaddr_decoded = nexaddr_decode(address);
    if let Ok(nexaddr) = nexaddr_decoded {
        return Ok((nexaddr.0, nexaddr.1));
    };
    let (cashaddr_err, base58_err) = bchaddr_decoded.expect_err("expected decode error");
    let nexaddr_err = nexaddr_decoded.expect_err("expected decode error");
    Err(anyhow!("{:?}", (cashaddr_err, base58_err, nexaddr_err)))
}

/**
 * Return scripthash of address *as it would be indexed by Rostrum*.
 * If there are token amounts encoded in address, these will be trimmed out.
 */
pub fn addr_to_scripthash(addr: &str) -> Result<ScriptHash> {
    let (payload, address_type) = decode_address(addr)?;

    let pubkey: Script = match address_type {
        version_byte_flags::TYPE_P2PKH => Builder::new()
            .push_opcode(opcodes::all::OP_DUP)
            .push_opcode(opcodes::all::OP_HASH160)
            .push_slice(&payload)
            .push_opcode(opcodes::all::OP_EQUALVERIFY)
            .push_opcode(opcodes::all::OP_CHECKSIG)
            .into_script(),

        version_byte_flags::TYPE_P2SH => Builder::new()
            .push_opcode(opcodes::all::OP_HASH160)
            .push_slice(&payload)
            .push_opcode(opcodes::all::OP_EQUAL)
            .into_script(),
        version_byte_flags::TYPE_SCRIPT_TEMPLATE => {
            let script_pubkey = Script::from(payload[1..].to_vec());
            let out = crate::nexa::transaction::TxOut {
                txout_type: TxOutType::TEMPLATE as u8,
                value: 0,
                script_pubkey,
            };
            match out.scriptpubkey_without_token() {
                Ok(Some(s)) => s,
                _ => {
                    // If we failed to trim a token out, just encoded at as we
                    // otherwise would.
                    out.script_pubkey
                }
            }
        }
        _ => return Err(anyhow!("Unknown address type {}", address_type)),
    };
    Ok(ScriptHash::from_script(&pubkey))
}

#[cfg(test)]
mod tests {
    use bitcoin_hashes::hex::{FromHex, ToHex};

    use super::*;

    #[test]
    fn test_addr_to_scripthash_p2pkh() {
        // Protocol specification test vector
        let scripthash = ScriptHash::from_hex(
            "8b01df4e368ea28f8dc0423bcf7a4923e3a12d307c875e47a0cfbf90b5c39161",
        )
        .unwrap();
        assert_eq!(
            scripthash,
            addr_to_scripthash("bitcoincash:qp3wjpa3tjlj042z2wv7hahsldgwhwy0rq9sywjpyy").unwrap()
        );

        assert_eq!(
            scripthash,
            addr_to_scripthash("1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa").unwrap()
        );
    }

    #[test]
    fn test_addr_to_scripthash_p2sh() {
        // eatbch
        let scripthash = ScriptHash::from_hex(
            "829ce9ce75a8a8a01bf27a7365655506614ef0b8f5a7ecbef19093951a73b686",
        )
        .unwrap();
        assert_eq!(
            scripthash,
            addr_to_scripthash("bitcoincash:pp8skudq3x5hzw8ew7vzsw8tn4k8wxsqsv0lt0mf3g").unwrap()
        );
        assert_eq!(
            scripthash,
            addr_to_scripthash("38ty1qB68gHsiyZ8k3RPeCJ1wYQPrUCPPr").unwrap()
        );
    }

    #[test]
    fn test_addr_to_scripthash_garbage() {
        assert!(addr_to_scripthash("garbage").is_err());
    }

    #[test]
    fn test_to_le_hex() {
        let hex = "829ce9ce75a8a8a01bf27a7365655506614ef0b8f5a7ecbef19093951a73b686";
        let scripthash: ScriptHash = ScriptHash::from_hex(hex).unwrap();
        assert_eq!(hex, scripthash.to_hex());
    }
}
