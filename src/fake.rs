use crate::store::{ReadStore, Row, WriteStore};
use rayon::prelude::*;

pub struct FakeStore;

impl ReadStore for FakeStore {
    fn get(&self, _key: &[u8]) -> Option<Vec<u8>> {
        None
    }
    fn scan(&self, _prefix: &[u8]) -> Vec<Row> {
        vec![]
    }
}

impl WriteStore for FakeStore {
    fn write<I: IntoParallelIterator<Item = Row>>(&self, _rows: I, _sync: bool) {}
    fn flush(&self) {}
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_fakestore() {
        use crate::fake;
        use crate::store::{ReadStore, Row, WriteStore};

        let store = fake::FakeStore {};
        store.write(
            vec![Row {
                key: b"k".to_vec().into_boxed_slice(),
                value: b"v".to_vec().into_boxed_slice(),
            }],
            true,
        );
        store.flush();
        // nothing was actually written
        assert!(store.get(b"").is_none());
        assert!(store.scan(b"").is_empty());
    }
}
