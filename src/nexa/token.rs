use std::convert::TryInto;
use std::ops::Index;

use bitcoincash::blockdata::script::read_uint;
use bitcoincash::{blockdata::opcodes, Script};

use crate::chaindef::TokenID;
use crate::utilscript::read_push_from_script;
use anyhow::Result;

pub const PARENT_GROUP_ID_LENGTH: usize = 32;
pub const OPRETURN_GROUP_PREFIX: u32 = 88888888;
pub const MAX_TICKER_LENGTH: usize = 8;

#[derive(Default)]
pub struct Token {
    pub hash: Option<TokenID>,
    pub ticker: Option<String>,
    pub name: Option<String>,
    pub document_url: Option<String>,
    pub document_hash: Option<[u8; 32]>,
}

/**
 * Serialize amount as encoded in Nexa
 */
pub fn serialize_amount(amount: i64) -> Vec<u8> {
    if amount < 0 {
        return bitcoincash::consensus::serialize::<u64>(&(amount as u64));
    }
    if amount <= u16::MAX as i64 {
        return bitcoincash::consensus::serialize::<u16>(&(amount as u16));
    }
    if amount <= u32::MAX as i64 {
        return bitcoincash::consensus::serialize::<u32>(&(amount as u32));
    }
    bitcoincash::consensus::serialize::<u64>(&(amount as u64))
}

/**
 * Deserialize amount as encoded in Nexa
 */
pub fn deserialize_amount(data: &[u8]) -> Result<i64> {
    let decoded = match data.len() {
        2 => bitcoincash::consensus::deserialize::<u16>(data).map(|x| x as u64),
        4 => bitcoincash::consensus::deserialize::<u32>(data).map(|x| x as u64),
        8 => bitcoincash::consensus::deserialize::<u64>(data),
        _ => bail!("Invalid length {}", data.len()),
    };
    match decoded {
        Err(e) => Err(anyhow::Error::from(e)),
        Ok(n) => Ok(n as i64),
    }
}

/**
 * Parse group transfer in a output. (This includes new token generation).
 *
 * Returns hash + amount (negative value if operation)
 */
pub fn parse_token_from_scriptpubkey(s: &Script) -> Option<(TokenID, i64)> {
    // At minimum, push op + 20 bytes + push op + 2 bytes amount
    if s.len() < 1 + 20 + 1 + 2 {
        return None;
    }

    if s.index(0) == &opcodes::all::OP_PUSHBYTES_0.to_u8() {
        // Not a group transaction
        return None;
    }

    let iter = s.as_bytes().iter();
    let (iter, tokenhash) = read_push_from_script(iter).ok()?;
    let tokenhash = tokenhash?;

    if tokenhash.len() < PARENT_GROUP_ID_LENGTH {
        // Minimum 32 bytes. Subgroups can be larger (up to consensus stack size).
        return None;
    }

    let (_, amount_serialized) = read_push_from_script(iter).ok()?;
    let amount_serialized = amount_serialized?;
    let amount = deserialize_amount(&amount_serialized).ok()?;

    match TokenID::new(tokenhash) {
        Ok(hash) => Some((hash, amount)),
        _ => None,
    }
}

/**
 * Parse group token details from a OP_RETURN
 */
pub fn parse_token_description(script: &Script) -> Result<Token> {
    let mut iter = script.as_ref().iter();
    if let Some(o) = iter.next() {
        if *o != 0x6a
        /* OP_RETURN */
        {
            bail!("Not a OP_RETURN script")
        }
    } else {
        bail!("Empty script")
    }

    let (iter, prefix) = read_push_from_script(iter)?;
    if let Some(p) = prefix {
        let prefix = read_uint(&p, 4).ok();
        if prefix != Some(OPRETURN_GROUP_PREFIX as usize) {
            return Ok(Token::default());
        }
    } else {
        return Ok(Token::default());
    };

    let (iter, ticker) = read_push_from_script(iter)?;
    let ticker = if let Some(t) = ticker {
        if t.len() <= MAX_TICKER_LENGTH {
            String::from_utf8(t).ok()
        } else {
            None
        }
    } else {
        None
    };

    let (iter, name) = read_push_from_script(iter)?;
    let name = if let Some(n) = name {
        String::from_utf8(n).ok()
    } else {
        None
    };

    let (iter, document_url) = read_push_from_script(iter)?;
    let document_url = if let Some(url) = document_url {
        String::from_utf8(url).ok()
    } else {
        None
    };

    let (_, document_hash) = read_push_from_script(iter)?;
    let document_hash = document_hash.unwrap_or_default();
    let document_hash = if document_hash.len() == 32 {
        let mut d: [u8; 32] = document_hash.try_into().unwrap();
        d.reverse();
        Some(d)
    } else {
        None
    };

    Ok(Token {
        hash: None,
        name,
        ticker,
        document_url,
        document_hash,
    })
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_serialize_amount() {
        assert_eq!(-42, deserialize_amount(&serialize_amount(-42)).unwrap());

        assert_eq!(
            u16::MAX as i64,
            deserialize_amount(&serialize_amount(u16::MAX as i64)).unwrap()
        );

        assert_eq!(
            u32::MAX as i64,
            deserialize_amount(&serialize_amount(u32::MAX as i64)).unwrap()
        );

        assert_eq!(
            i64::MAX as i64,
            deserialize_amount(&serialize_amount(i64::MAX as i64)).unwrap()
        );
    }

    #[test]
    fn test_parse_scriptpubkey() {
        let hex = "202ab2bcb6b2fb547edcdde5011e947c84b051f9767dc0758157b304ec452000000872790000000000fc511490793b86d7bdf8bd36ec795d0082ee41fbeff7e3";
        let script = Script::from(hex::decode(hex).unwrap());
        let (hash, amount) = parse_token_from_scriptpubkey(&script).unwrap();
        assert_eq!(
            hash.to_string(),
            "2ab2bcb6b2fb547edcdde5011e947c84b051f9767dc0758157b304ec45200000"
        );
        assert_eq!(amount, -288230376151680654);
    }

    #[test]
    fn test_parse_opreturn() {
        let hex = "6a0438564c0504444f4745084d75636820576f771368747470733a2f2f6578616d706c652e6f726720efbead0b00000000000000000000000000000000000000000000000000000000";
        let script = Script::from(hex::decode(hex).unwrap());
        let token = parse_token_description(&script).unwrap();
        assert_eq!("DOGE", token.ticker.unwrap());
        assert_eq!("Much Wow", token.name.unwrap());
        assert_eq!("https://example.org", token.document_url.unwrap());
        assert_eq!(
            hex::decode("000000000000000000000000000000000000000000000000000000000badbeef")
                .unwrap(),
            token.document_hash.unwrap()
        );
    }

    #[test]
    fn test_parse_subgroup_sciptpubkey() {
        let hex = "28442dbb752c2fca49d5f6901a14d953e12dd4a84862f556005de47f5d03110000c800000000000000022a0051144d8f8df0ed540551c38cdf8443dcb549a2f93c2b";
        let script = Script::from(hex::decode(hex).unwrap());
        let (token, amount) = parse_token_from_scriptpubkey(&script).unwrap();
        assert_eq!(
            "442dbb752c2fca49d5f6901a14d953e12dd4a84862f556005de47f5d03110000c800000000000000",
            token.to_hex()
        );
        assert_eq!(42, amount);
    }

    #[test]
    fn test_parse_opreturn_longname() {
        let hex = "6a0438564c050568656c6c6f2261206e616d65206f66206120746f6b656e20776974682061206c6f6e67206e616d650000";
        let script = Script::from(hex::decode(hex).unwrap());
        let token = parse_token_description(&script).unwrap();
        assert_eq!("a name of a token with a long name", token.name.unwrap());
    }
}
