use crate::store::Row;

pub mod cashaccount;
pub mod headerindex;
pub mod heightindex;
pub mod inputindex;
pub mod outputindex;
pub mod outputtokenindex;
pub mod scripthashindex;
pub mod tokenoutputindex;

pub trait DBRow {
    fn to_row(&self) -> Row;
    fn from_row(row: &Row) -> Self;
}
