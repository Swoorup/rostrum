use std::convert::TryInto;

use crate::chaindef::OutPointHash;
use crate::encode::decode_varint_usize;
use crate::encode::encode_varint_usize;
use crate::store::Row;
use bitcoincash::hashes::Hash;
use bitcoincash::Txid;

#[derive(Serialize, Deserialize)]
pub struct InputIndexKey {
    pub code: u8,
    pub outputhash: [u8; 32],
}

#[derive(Serialize, Deserialize)]
pub struct InputIndexRow {
    key: InputIndexKey,
    txid: Vec<u8>,
    index: Vec<u8>,
}

const INPUT_INDEX_CODE: u8 = b'I';

impl InputIndexRow {
    pub fn new(outputhash: [u8; 32], txid: Txid, input_index: usize) -> InputIndexRow {
        InputIndexRow {
            key: InputIndexKey {
                code: INPUT_INDEX_CODE,
                outputhash,
            },
            txid: txid.to_vec(),
            index: encode_varint_usize(input_index),
        }
    }

    pub fn filter_by_outputhash(outputhash: &OutPointHash) -> Vec<u8> {
        bincode::serialize(&InputIndexKey {
            code: INPUT_INDEX_CODE,
            outputhash: outputhash.to_vec().try_into().unwrap(),
        })
        .unwrap()
    }

    pub fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.key).unwrap().into_boxed_slice(),
            value: bincode::serialize(&[self.txid.to_vec(), self.index.clone()])
                .unwrap()
                .into_boxed_slice(),
        }
    }

    pub fn from_row(row: &Row) -> InputIndexRow {
        let [txid, index]: [Vec<u8>; 2] =
            bincode::deserialize(&row.value).expect("failed to parse OutputIndexRow");
        InputIndexRow {
            key: bincode::deserialize(&row.key).expect("failed to parse OutputIndexKey"),
            txid,
            index,
        }
    }

    pub fn txid(&self) -> Txid {
        Txid::from_slice(&self.txid).unwrap()
    }

    pub fn index(&self) -> usize {
        decode_varint_usize(&self.index)
    }
}
