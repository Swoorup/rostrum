use std::convert::TryInto;

use bitcoin_hashes::Hash;

use crate::chaindef::{OutPointHash, ScriptHash};
use crate::store::Row;

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum OutputFlags {
    None = 0,
    HasTokens = 1,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ScriptHashIndexKey {
    pub code: u8,
    pub scripthash: [u8; 32],
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ScriptHashIndexRow {
    key: ScriptHashIndexKey,
    flags: u8,
    outpointhash: [u8; 32],
}

const SCRIPTHASH_INDEX_CODE: u8 = b'O';

impl ScriptHashIndexRow {
    pub fn new(
        scripthash: &ScriptHash,
        outpointhash: &OutPointHash,
        flags: OutputFlags,
    ) -> ScriptHashIndexRow {
        ScriptHashIndexRow {
            key: ScriptHashIndexKey {
                code: SCRIPTHASH_INDEX_CODE,
                scripthash: scripthash
                    .to_vec()
                    .try_into()
                    .expect("failed to encode ScriptHash"),
            },
            flags: flags as u8,
            outpointhash: outpointhash
                .to_vec()
                .try_into()
                .expect("failed to encode OutPointHash"),
        }
    }

    pub fn filter_by_scripthash(scripthash: [u8; 32]) -> Vec<u8> {
        bincode::serialize(&ScriptHashIndexKey {
            code: SCRIPTHASH_INDEX_CODE,
            scripthash,
        })
        .unwrap()
    }

    pub fn filter_by_scripthash_with_token(scripthash: [u8; 32]) -> Vec<u8> {
        Self::filter_by_scripthash(scripthash)
            .into_iter()
            .chain(std::iter::once(OutputFlags::HasTokens as u8))
            .collect()
    }

    pub fn to_row(&self) -> Row {
        // Hack: Encoding it all into key results in also being encoded to value. See unit test.
        Row {
            key: bincode::serialize(&self).unwrap().into_boxed_slice(),
            value: vec![].into_boxed_slice(),
        }
    }

    pub fn from_row(row: &Row) -> ScriptHashIndexRow {
        bincode::deserialize(&row.key).expect("failed to read ScriptHashIndexRow")
    }

    pub fn outpointhash(&self) -> OutPointHash {
        OutPointHash::from_slice(&self.outpointhash).unwrap()
    }

    pub fn outpointhash_raw(&self) -> [u8; 32] {
        self.outpointhash
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_to_from_row() {
        let row = ScriptHashIndexRow::new(
            &ScriptHash::hash(&[42]),
            &OutPointHash::hash(&[11]),
            OutputFlags::HasTokens,
        );
        let decoded_row = ScriptHashIndexRow::from_row(&row.to_row());
        assert_eq!(row, decoded_row);
    }

    #[test]
    fn test_filter_by_scripthash_with_token() {
        let scripthash = ScriptHash::hash(&[42]);
        let row_with_token = ScriptHashIndexRow::new(
            &scripthash,
            &OutPointHash::hash(&[11]),
            OutputFlags::HasTokens,
        )
        .to_row();
        let row_without_token =
            ScriptHashIndexRow::new(&scripthash, &OutPointHash::hash(&[11]), OutputFlags::None)
                .to_row();

        let filter_token = ScriptHashIndexRow::filter_by_scripthash_with_token(
            scripthash.to_vec().try_into().unwrap(),
        );
        let all = ScriptHashIndexRow::filter_by_scripthash(scripthash.to_vec().try_into().unwrap());

        let starts_with = |a: &Row, b: &Vec<u8>| -> bool {
            a.key.to_vec().iter().zip(b.iter()).all(|(x, y)| x == y)
        };
        assert!(starts_with(&row_with_token, &filter_token));
        assert!(!starts_with(&row_without_token, &filter_token));

        assert!(starts_with(&row_with_token, &all));
        assert!(starts_with(&row_without_token, &all));
    }
}
