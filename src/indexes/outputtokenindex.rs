use std::convert::TryInto;

use crate::chaindef::OutPointHash;
use crate::chaindef::TokenID;
use crate::nexa::token::deserialize_amount;
use crate::nexa::token::serialize_amount;
use crate::store::Row;
use bitcoincash::hashes::Hash;

use super::DBRow;

#[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
pub struct OutputTokenIndexKey {
    pub code: u8,
    pub outpoint_hash: [u8; 32],
    pub token_id: Vec<u8>,
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
pub struct OutputTokenIndexRow {
    key: OutputTokenIndexKey,
    // consensus encoded variable length
    token_amount: Vec<u8>,
}

const OUTPUT_TOKEN_INDEX_CODE: u8 = b'K';

/**
 * Indexes `outputhash` -> `tokenid`
 */
impl OutputTokenIndexRow {
    pub fn new(outpoint_hash: &OutPointHash, token_id: TokenID, token_amount: i64) -> Self {
        Self {
            key: OutputTokenIndexKey {
                code: OUTPUT_TOKEN_INDEX_CODE,
                outpoint_hash: outpoint_hash.to_vec().try_into().unwrap(),
                token_id: token_id.into_vec(),
            },
            token_amount: serialize_amount(token_amount),
        }
    }

    pub fn filter_by_outpointhash(hash: &OutPointHash) -> Vec<u8> {
        std::iter::once(OUTPUT_TOKEN_INDEX_CODE)
            .chain(hash.iter().copied())
            .collect()
    }

    pub fn filter_by_outpointhash_raw(outpoint_hash: &[u8; 32]) -> Vec<u8> {
        std::iter::once(OUTPUT_TOKEN_INDEX_CODE)
            .chain(outpoint_hash.iter().copied())
            .collect()
    }

    pub fn filter_by_outpointhash_and_token(hash: &OutPointHash, token_id: TokenID) -> Vec<u8> {
        bincode::serialize(&OutputTokenIndexKey {
            code: OUTPUT_TOKEN_INDEX_CODE,
            outpoint_hash: hash.to_vec().try_into().unwrap(),
            token_id: token_id.into_vec(),
        })
        .unwrap()
    }

    pub fn outpoint_hash(&self) -> OutPointHash {
        OutPointHash::from_slice(&self.key.outpoint_hash).expect("failed to decode OutPointHash")
    }

    pub fn token_id(&self) -> TokenID {
        TokenID::new(self.key.token_id.clone()).unwrap()
    }

    pub fn into_token_id(self) -> TokenID {
        TokenID::new(self.key.token_id).unwrap()
    }

    pub fn token_amount(&self) -> i64 {
        deserialize_amount(&self.token_amount).unwrap()
    }
}

impl DBRow for OutputTokenIndexRow {
    fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.key).unwrap().into_boxed_slice(),
            value: bincode::serialize(&self.token_amount)
                .unwrap()
                .into_boxed_slice(),
        }
    }

    fn from_row(row: &Row) -> Self {
        Self {
            key: bincode::deserialize(&row.key).expect("failed to read OutputTokenIndexRow"),
            token_amount: bincode::deserialize(&row.value)
                .expect("failed to deserialize OutputTokenIndexRow"),
        }
    }
}

#[cfg(test)]
mod tests {
    use bitcoin_hashes::hex::ToHex;

    use super::*;
    #[test]
    fn test_to_from_row() {
        let row = OutputTokenIndexRow::new(
            &OutPointHash::hash(&[42]),
            TokenID::new(vec![0xAA; 32]).unwrap(),
            42,
        );
        let decoded_row = OutputTokenIndexRow::from_row(&row.to_row());
        assert_eq!(row, decoded_row);
    }

    #[test]
    fn test_filter_by_outpointhash_and_token() {
        let token_a = TokenID::new(vec![0xAA; 32]).unwrap();
        let token_b = TokenID::new(vec![0xBB; 32]).unwrap();
        let outpointhash = OutPointHash::hash(&[42]);
        let row_with_token_a =
            OutputTokenIndexRow::new(&outpointhash, token_a.clone(), 42).to_row();
        let row_with_token_b =
            OutputTokenIndexRow::new(&outpointhash, token_b.clone(), 24).to_row();

        let filter_token_a =
            OutputTokenIndexRow::filter_by_outpointhash_and_token(&outpointhash, token_a);
        let all = OutputTokenIndexRow::filter_by_outpointhash(&outpointhash);
        let all2 = OutputTokenIndexRow::filter_by_outpointhash_raw(
            &outpointhash.to_vec().try_into().unwrap(),
        );

        let starts_with = |a: &Row, b: &Vec<u8>| -> bool {
            a.key.to_vec().iter().zip(b.iter()).all(|(x, y)| x == y)
        };
        assert!(starts_with(&row_with_token_a, &filter_token_a));
        assert!(!starts_with(&row_with_token_b, &filter_token_a));

        assert!(starts_with(&row_with_token_a, &all));
        assert!(starts_with(&row_with_token_b, &all));
        assert!(starts_with(&row_with_token_a, &all2));
        assert!(starts_with(&row_with_token_b, &all2));
    }

    #[test]
    fn test_filter_matches_key() {
        let token = TokenID::new(vec![0xAA; 32]).unwrap();
        let outpointhash = OutPointHash::hash(&[42]);
        let row = OutputTokenIndexRow::new(&outpointhash, token.clone(), 42).to_row();
        let filter = OutputTokenIndexRow::filter_by_outpointhash_and_token(&outpointhash, token);

        assert_eq!(row.key.to_vec().to_hex(), filter.to_hex());
    }
}
