use crate::store::Row;
use bitcoincash::Txid;
use sha2::{Digest, Sha256};
use std::convert::TryInto;

fn compute_accountname_hash(accountname: &[u8], blockheight: u32) -> [u8; 32] {
    let mut sha2 = Sha256::new();
    sha2.update(accountname);
    sha2.update(&blockheight.to_be_bytes());
    sha2.finalize().into()
}

#[derive(Serialize, Deserialize)]
pub struct TxCashAccountKey {
    code: u8,
    account_hash: [u8; 32],
}

#[derive(Serialize, Deserialize)]
pub struct TxCashAccountRow {
    key: TxCashAccountKey,
    pub txid: [u8; 32],
}

impl TxCashAccountRow {
    pub fn new(txid: &Txid, accountname: &[u8], blockheight: u32) -> TxCashAccountRow {
        TxCashAccountRow {
            key: TxCashAccountKey {
                code: b'C',
                account_hash: compute_accountname_hash(accountname, blockheight),
            },
            txid: txid.to_vec().try_into().unwrap(),
        }
    }

    pub fn filter(accountname: &[u8], blockheight: u32) -> Vec<u8> {
        bincode::serialize(&TxCashAccountKey {
            code: b'C',
            account_hash: compute_accountname_hash(accountname, blockheight),
        })
        .unwrap()
    }

    pub fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self).unwrap().into_boxed_slice(),
            value: vec![].into_boxed_slice(),
        }
    }

    pub fn from_row(row: &Row) -> TxCashAccountRow {
        bincode::deserialize(&row.key).expect("failed to parse TxCashAccountRow")
    }
}
