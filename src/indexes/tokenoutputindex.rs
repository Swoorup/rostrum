use std::convert::TryInto;

use crate::chaindef::OutPointHash;
use crate::chaindef::TokenID;
use crate::store::Row;
use bitcoincash::hashes::Hash;

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct TokenOutputIndexKey {
    pub code: u8,
    pub token_id: Vec<u8>,
    outpoint_hash: [u8; 32],
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct TokenOutputIndexRow {
    key: TokenOutputIndexKey,
}

const TOKEN_OUTPUT_INDEX_CODE: u8 = b'A';

/**
 * Indexes `tokenid` -> `outpointhash`
 */
impl TokenOutputIndexRow {
    pub fn new(token_id: TokenID, outpoint_hash: &OutPointHash) -> Self {
        Self {
            key: TokenOutputIndexKey {
                code: TOKEN_OUTPUT_INDEX_CODE,
                token_id: token_id.into_vec(),
                outpoint_hash: outpoint_hash.to_vec().try_into().unwrap(),
            },
        }
    }

    pub fn filter_by_token_id(token_id: &TokenID) -> Vec<u8> {
        std::iter::once(vec![TOKEN_OUTPUT_INDEX_CODE])
            .chain(bincode::serialize(token_id.as_vec()).into_iter())
            .flatten()
            .collect()
    }

    pub fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.key).unwrap().into_boxed_slice(),
            value: vec![].into_boxed_slice(),
        }
    }

    pub fn from_row(row: &Row) -> Self {
        Self {
            key: bincode::deserialize(&row.key).expect("failed to parse TokenOutputIndexKey"),
        }
    }

    pub fn outpoint_hash(&self) -> OutPointHash {
        OutPointHash::from_slice(&self.key.outpoint_hash).expect("failed to decode OutPointHash")
    }

    pub fn outpoint_hash_raw(&self) -> &[u8; 32] {
        &self.key.outpoint_hash
    }

    pub fn token_id(&self) -> TokenID {
        TokenID::new(self.key.token_id.clone()).unwrap()
    }

    pub fn into_token_id(self) -> TokenID {
        TokenID::new(self.key.token_id).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_to_from_row() {
        let row = TokenOutputIndexRow::new(
            TokenID::new(vec![0xaa; 32]).unwrap(),
            &OutPointHash::hash(&[11]),
        );
        let decoded_row = TokenOutputIndexRow::from_row(&row.to_row());
        assert_eq!(row, decoded_row);
    }
}
