use std::convert::TryInto;

use crate::chaindef::OutPointHash;
use crate::encode::encode_varint_u32;
use crate::encode::encode_varint_u64;
use crate::store::Row;
use bitcoincash::hashes::Hash;
use bitcoincash::Txid;

use crate::encode::decode_varint_u32;
use crate::encode::decode_varint_u64;

use super::DBRow;

#[derive(Serialize, Deserialize)]
pub struct OutputIndexKey {
    pub code: u8,
    pub hash: [u8; 32],
}

#[derive(Serialize, Deserialize)]
pub struct OutputIndexRow {
    key: OutputIndexKey,
    txid: Vec<u8>,
    index: Vec<u8>,
    value: Vec<u8>,
}

const OUTPUT_INDEX_CODE: u8 = b'H';

impl OutputIndexRow {
    pub fn new(
        txid: &Txid,
        outpoint_hash: &OutPointHash,
        output_value: u64,
        output_index: u32,
    ) -> OutputIndexRow {
        OutputIndexRow {
            key: OutputIndexKey {
                code: OUTPUT_INDEX_CODE,
                hash: outpoint_hash.to_vec().try_into().unwrap(),
            },
            txid: txid.to_vec(),
            index: encode_varint_u32(output_index),
            value: encode_varint_u64(output_value),
        }
    }

    pub fn filter_by_outpointhash(hash: &OutPointHash) -> Vec<u8> {
        bincode::serialize(&OutputIndexKey {
            code: OUTPUT_INDEX_CODE,
            hash: hash.to_vec().try_into().unwrap(),
        })
        .unwrap()
    }

    pub fn filter_by_outpointhash_raw(hash: [u8; 32]) -> Vec<u8> {
        bincode::serialize(&OutputIndexKey {
            code: OUTPUT_INDEX_CODE,
            hash,
        })
        .unwrap()
    }

    pub fn hash(&self) -> OutPointHash {
        OutPointHash::from_slice(&self.key.hash).expect("failed to decode OutPutIndexKey::hash")
    }

    pub fn hash_raw(&self) -> &[u8; 32] {
        &self.key.hash
    }

    pub fn txid(&self) -> Txid {
        Txid::from_slice(&self.txid).unwrap()
    }

    pub fn index(&self) -> u32 {
        decode_varint_u32(&self.index)
    }

    pub fn value(&self) -> u64 {
        decode_varint_u64(&self.value)
    }
}

impl DBRow for OutputIndexRow {
    fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.key).unwrap().into_boxed_slice(),
            value: bincode::serialize(&[
                self.txid.to_vec(),
                self.index.clone(),
                self.value.clone(),
            ])
            .unwrap()
            .into_boxed_slice(),
        }
    }

    fn from_row(row: &Row) -> OutputIndexRow {
        let [txid, index, value]: [Vec<u8>; 3] =
            bincode::deserialize(&row.value).expect("failed to parse OutputIndexRow");
        OutputIndexRow {
            key: bincode::deserialize(&row.key).expect("failed to parse OutputIndexKey"),
            txid,
            index,
            value,
        }
    }
}
