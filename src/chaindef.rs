use std::fmt::Display;

use crate::{impl_nexa_hashencode, nexa::token::PARENT_GROUP_ID_LENGTH};
use anyhow::Result;
use bitcoin_hashes::{sha256, Hash};
use bitcoincash::Script;

#[cfg(feature = "nexa")]
pub type BlockHeader = crate::nexa::block::BlockHeader;
#[cfg(not(feature = "nexa"))]
pub type BlockHeader = bitcoincash::blockdata::block::BlockHeader;

#[cfg(feature = "nexa")]
pub type BlockHash = crate::nexa::hash_types::BlockHash;
#[cfg(not(feature = "nexa"))]
pub type BlockHash = bitcoincash::hash_types::BlockHash;

#[cfg(feature = "nexa")]
pub type Block = crate::nexa::block::Block;
#[cfg(not(feature = "nexa"))]
pub type Block = bitcoincash::blockdata::block::Block;

#[cfg(feature = "nexa")]
pub type OutPoint = crate::nexa::transaction::OutPoint;
#[cfg(not(feature = "nexa"))]
pub type OutPoint = bitcoincash::blockdata::transaction::OutPoint;

#[cfg(feature = "nexa")]
pub type TxIn = crate::nexa::transaction::TxIn;
#[cfg(not(feature = "nexa"))]
pub type TxIn = bitcoincash::blockdata::transaction::TxIn;

#[cfg(feature = "nexa")]
pub type TxOut = crate::nexa::transaction::TxOut;
#[cfg(not(feature = "nexa"))]
pub type TxOut = bitcoincash::blockdata::transaction::TxOut;

#[cfg(feature = "nexa")]
pub type Transaction = crate::nexa::transaction::Transaction;
#[cfg(not(feature = "nexa"))]
pub type Transaction = bitcoincash::blockdata::transaction::Transaction;

hash_newtype!(
    OutPointHash,
    sha256::Hash,
    32,
    doc = "Hash pointing to a outpoint (utxo). The hash of txid & output index."
);
impl_nexa_hashencode!(OutPointHash);

hash_newtype!(
    ScriptHash,
    sha256::Hash,
    32,
    doc = "Hash of a Bitcoin Script",
    true
);

impl_nexa_hashencode!(ScriptHash);

impl ScriptHash {
    pub fn from_script(s: &Script) -> ScriptHash {
        ScriptHash::hash(&s[..])
    }

    /**
     * ScriptHash where any token amounts are replaced with OP_0.
     */
    #[cfg(feature = "nexa")]
    pub fn normalized_from_txout(o: &TxOut) -> ScriptHash {
        match o.scriptpubkey_without_token() {
            Ok(Some(new_script)) => Self::from_script(&new_script),
            _ => Self::from_script(&o.script_pubkey),
        }
    }

    #[cfg(not(feature = "nexa"))]
    pub fn normalized_from_txout(o: &TxOut) -> ScriptHash {
        // TODO: Support BCH tokens
        Self::from_script(&o.script_pubkey)
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Hash)]
pub struct TokenID {
    data: Vec<u8>,
}

impl TokenID {
    pub fn new(data: Vec<u8>) -> Result<Self> {
        if data.len() < PARENT_GROUP_ID_LENGTH {
            bail!(
                "Token ID is too short ({} < {}",
                data.len(),
                PARENT_GROUP_ID_LENGTH
            )
        }
        Ok(Self { data })
    }

    pub fn from_hex(hex: &str) -> Result<Self> {
        Self::new(hex::decode(hex)?)
    }

    pub fn into_vec(self) -> Vec<u8> {
        self.data
    }

    pub fn as_vec(&self) -> &Vec<u8> {
        &self.data
    }

    pub fn to_hex(&self) -> String {
        hex::encode(&self.data)
    }
}

impl Display for TokenID {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.to_hex())
    }
}

impl serde::Serialize for TokenID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_hex())
    }
}

#[cfg(not(feature = "nexa"))]
pub(crate) fn regtest_genesis_block() -> Block {
    bitcoincash::blockdata::constants::genesis_block(bitcoincash::Network::Regtest)
}

#[cfg(feature = "nexa")]
pub fn regtest_genesis_block() -> Block {
    todo!()
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use crate::nexa::token::PARENT_GROUP_ID_LENGTH;

    use super::*;

    #[test]
    fn test_tokenid_serialize() {
        let dummy_id = TokenID::new(vec![0xAA; PARENT_GROUP_ID_LENGTH]).unwrap();
        assert_eq!(
            format!("\"{}\"", dummy_id.to_hex()),
            json!(dummy_id).to_string()
        )
    }

    #[test]
    fn test_tokenid_serialize_in_map() {
        let mut map: HashMap<TokenID, i64> = HashMap::new();
        let dummy_id = TokenID::new(vec![0xAA; PARENT_GROUP_ID_LENGTH]).unwrap();
        map.insert(dummy_id.clone(), 42);
        assert_eq!(
            format!("{{\"{}\":42}}", dummy_id.to_hex()),
            json!(map).to_string()
        );
    }
}
