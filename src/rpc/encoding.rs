use crate::chaindef::BlockHash;
use anyhow::{Context, Result};
use bitcoincash::hashes::hex::{FromHex, ToHex};
use bitcoincash::hashes::Hash;
use serde_json::Value;

pub fn hex_to_txid<T: Hash>(value: &Value) -> Result<T> {
    T::from_hex(
        value
            .as_str()
            .context(format!("non-string txid value: {}", value))?,
    )
    .context(format!("non-hex txid value: {}", value))
}

pub fn txid_to_hex<T: Hash>(value: &T) -> String {
    value.to_hex()
}

pub fn hex_to_blockhash(value: &Value) -> Result<BlockHash> {
    BlockHash::from_hex(
        value
            .as_str()
            .context(format!("non-string blockhash value: {}", value))?,
    )
    .context(format!("non-hex blockhash value: {}", value))
}

pub fn blockhash_to_hex(value: &BlockHash) -> String {
    value.to_hex()
}
