use crate::chaindef::{BlockHash, ScriptHash};
use crate::errors::rpc_not_found;
use crate::indexes::scripthashindex::OutputFlags;
use crate::mempool::MEMPOOL_HEIGHT;
use crate::query::Query;
use crate::rpc::encoding::blockhash_to_hex;
use crate::timeout::TimeoutTrigger;
use anyhow::Result;
use bitcoin_hashes::Hash;
use bitcoincash::hashes::hex::ToHex;
use serde_json::Value;

pub fn get_balance(
    query: &Query,
    scripthash: ScriptHash,
    timeout: &TimeoutTrigger,
) -> Result<Value> {
    let status = query.status(scripthash, OutputFlags::None, timeout)?;
    Ok(json!({
        "confirmed": status.confirmed_balance(),
        "unconfirmed": status.mempool_balance()
    }))
}

pub fn get_first_use(query: &Query, scripthash: ScriptHash) -> Result<Value> {
    let firstuse = query.scripthash_first_use(scripthash)?;

    if firstuse.is_none() {
        return Err(rpc_not_found(format!(
            "scripthash '{}' not found",
            scripthash
        )));
    }
    let firstuse = firstuse.unwrap();

    let blockhash = if firstuse.0 == MEMPOOL_HEIGHT {
        BlockHash::all_zeros()
    } else {
        let h = query.get_headers(&[firstuse.0 as usize]);
        if h.is_empty() {
            warn!("expected to find header for height {}", firstuse.0);
            BlockHash::all_zeros()
        } else {
            h[0].block_hash()
        }
    };

    let height = if firstuse.0 == MEMPOOL_HEIGHT {
        0
    } else {
        firstuse.0
    };

    Ok(json!({
        "block_hash": blockhash_to_hex(&blockhash),
        "height": height,
        "block_height": height, // deprecated
        "tx_hash": firstuse.1.to_hex()
    }))
}

pub fn get_history(
    query: &Query,
    scripthash: ScriptHash,
    timeout: &TimeoutTrigger,
) -> Result<Value> {
    let status = query.status(scripthash, OutputFlags::None, timeout)?;
    Ok(json!(status.history(None)))
}

pub fn get_mempool(
    query: &Query,
    scripthash: ScriptHash,
    timeout: &TimeoutTrigger,
) -> Result<Value> {
    let status = query.status_mempool(scripthash, OutputFlags::None, timeout)?;
    Ok(json!(status.history(None)))
}

pub fn listunspent(
    query: &Query,
    scripthash: ScriptHash,
    timeout: &TimeoutTrigger,
) -> Result<Value> {
    Ok(json!(query
        .status(scripthash, OutputFlags::None, timeout,)?
        .unspent(None)))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::encode::compute_output_hash;
    use crate::mempool::ConfirmationState;
    use crate::query::primitives::FundingOutput;
    use bitcoincash::hash_types::Txid;
    use bitcoincash::hashes::hex::FromHex;
    use serde_json::from_str;

    #[derive(Serialize, Deserialize)]
    struct Unspent {
        height: u32,
        tx_pos: u32,
        tx_hash: String,
        value: u64,
    }

    #[cfg(feature = "nexa")]
    fn create_out(height: Option<u32>, txid: &str) -> FundingOutput {
        use crate::nexa::hash_types::TxIdem;

        FundingOutput {
            outpoint_hash: compute_output_hash(&TxIdem::from_hex(txid).unwrap(), 0),
            vout: 0,
            txid: Txid::from_hex(txid).unwrap(),
            height,
            value: 2020,
            state: ConfirmationState::InMempool,
            token_id: None,
            token_amount: None,
        }
    }

    #[cfg(not(feature = "nexa"))]
    fn create_out(height: Option<u32>, txid: &str) -> FundingOutput {
        let txid = Txid::from_hex(txid).unwrap();
        FundingOutput {
            outpoint_hash: compute_output_hash(&txid, 0),
            vout: 0,
            txid: txid,
            height,
            value: 2020,
            state: ConfirmationState::InMempool,
            token_id: None,
            token_amount: None,
        }
    }

    #[test]
    fn test_output_to_json_mempool() {
        // Mempool height is 0 in the json API
        let out = create_out(None, &Txid::all_zeros().to_hex());
        let res: Unspent = from_str(&json!(&out).to_string()).unwrap();
        assert_eq!(0, res.height);

        // Confirmed at block 5000
        let out = create_out(Some(5000), &Txid::all_zeros().to_hex());
        let res: Unspent = from_str(&json!(&out).to_string()).unwrap();
        assert_eq!(5000, res.height);
    }

    #[test]
    fn test_output_to_json_txid() {
        let hex = "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeffffffffffffffffffffffffffffffff";
        let out = create_out(Some(1), hex);
        let res: Unspent = from_str(&json!(&out).to_string()).unwrap();
        assert_eq!(hex, res.tx_hash);
    }
}
