use std::{collections::HashSet, sync::Arc};

use bitcoin_hashes::hex::ToHex;
use serde_json::Value;

use crate::{
    chaindef::{ScriptHash, TokenID},
    encode::output_hash,
    errors::rpc_invalid_params,
    indexes::{outputtokenindex::OutputTokenIndexRow, scripthashindex::OutputFlags},
    mempool::MEMPOOL_HEIGHT,
    nexa::token::{parse_token_description, parse_token_from_scriptpubkey},
    query::{self, token::find_token_genesis, HistoryItem, Query},
    scripthash::addr_to_scripthash,
    timeout::TimeoutTrigger,
};
use anyhow::Result;

use crate::rpc::parseutil::{str_from_value, tokenid_from_value};

use super::parseutil::hash_from_value;

pub struct TokenRPC {
    query: Arc<Query>,
}

impl TokenRPC {
    pub fn new(query: Arc<Query>) -> Self {
        Self { query }
    }

    pub fn address_get_balance(&self, params: &[Value], timeout: &TimeoutTrigger) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let scripthash = addr_to_scripthash(&addr)?;
        self.get_balance(scripthash, timeout)
    }

    pub fn address_get_history(&self, params: &[Value], timeout: &TimeoutTrigger) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let scripthash = addr_to_scripthash(&addr)?;
        let token = tokenid_from_value(params.get(2))?;

        self.get_scripthash_history(scripthash, token, timeout)
    }

    pub(crate) fn address_get_mempool(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let scripthash = addr_to_scripthash(&addr)?;
        let token = tokenid_from_value(params.get(2))?;
        self.get_scripthash_mempool(scripthash, token, timeout)
    }

    pub(crate) fn address_listunspent(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let addr = str_from_value(params.get(0), "address")?;
        let scripthash = addr_to_scripthash(&addr)?;
        let token = tokenid_from_value(params.get(2))?;
        self.listunspent(scripthash, token, timeout)
    }

    pub(crate) fn scripthash_get_balance(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        self.get_balance(scripthash, timeout)
    }

    pub(crate) fn scripthash_get_history(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let scripthash: ScriptHash = hash_from_value(params.get(0))?;
        let token = tokenid_from_value(params.get(2))?;
        self.get_scripthash_history(scripthash, token, timeout)
    }

    pub(crate) fn scripthash_get_mempool(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        let token = tokenid_from_value(params.get(2))?;

        self.get_scripthash_mempool(scripthash, token, timeout)
    }

    pub(crate) fn scripthash_listunspent(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let scripthash = hash_from_value::<ScriptHash>(params.get(0))?;
        let token = tokenid_from_value(params.get(2))?;
        self.listunspent(scripthash, token, timeout)
    }

    pub(crate) fn token_transaction_history(
        &self,
        params: &[Value],
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let token_id = match tokenid_from_value(params.get(0))? {
            Some(t) => t,
            None => return Err(rpc_invalid_params("token_id missing".to_string())),
        };

        let confirmed_history =
            query::token::get_token_history(&token_id, self.query.confirmed_index(), timeout)?;
        let mempool_history = query::token::get_token_history(
            &token_id,
            self.query.unconfirmed_index().index(),
            timeout,
        )?;

        let as_history_items = mempool_history
            .iter()
            .chain(confirmed_history.iter())
            .map(|h| HistoryItem {
                tx_hash: h.0,
                height: if h.1 == MEMPOOL_HEIGHT { 0 } else { h.1 as i32 },
                fee: None,
            });

        Ok(json!({
            "cursor": None::<String>,
            "history": as_history_items.collect::<Vec<HistoryItem>>()
        }))
    }

    pub(crate) fn genesis_info(
        &self,
        params: &[Value],
        _timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        use crate::indexes::DBRow;

        let token_id = match tokenid_from_value(params.get(0))? {
            Some(t) => t,
            None => return Err(rpc_invalid_params("token_id missing".to_string())),
        };
        let (tx, height) =
            find_token_genesis(&token_id, self.query.confirmed_index(), self.query.tx())?;

        let tokens_out: HashSet<TokenID> = tx
            .output
            .iter()
            .filter_map(|o| {
                parse_token_from_scriptpubkey(&o.script_pubkey).map(|(token_id, _)| token_id)
            })
            .collect();

        let tokens_in: HashSet<TokenID> = tx
            .input
            .iter()
            .filter_map(|i| {
                let prefix =
                    OutputTokenIndexRow::filter_by_outpointhash(&output_hash(&i.previous_output));
                self.query
                    .confirmed_index()
                    .scan(&prefix)
                    .iter()
                    .map(OutputTokenIndexRow::from_row)
                    .map(|r| r.into_token_id())
                    .next()
            })
            .collect();

        if tokens_in.contains(&token_id) {
            bail!(
                "Expected {} to be genesis tx for {}, but it also had token as input.",
                tx.txid(),
                token_id,
            );
        }

        let genesis_outputs = tokens_out.difference(&tokens_in);
        let token = if genesis_outputs.count() == 1 {
            // Ours is the only token genesis output of the transaction.
            // If there is a OP_RETURN with token description, it belongs to this token.
            let op_return = tx.output.iter().find(|o| o.script_pubkey.is_op_return());
            op_return.map(|o| parse_token_description(&o.script_pubkey).unwrap_or_default())
        } else {
            None
        };

        let [_, ticker, name, document_url, document_hash] =
            token.map_or([None, None, None, None, None], |t| {
                [
                    t.hash.map(|h| h.to_hex()),
                    t.ticker,
                    t.name,
                    t.document_url,
                    t.document_hash.map(|d| d.to_hex()),
                ]
            });

        #[allow(unused_mut)]
        let mut tx_details = json!({
            "txid": tx.txid().to_hex(),
            "token_id_hex": token_id,
            "ticker": ticker,
            "name": name,
            "document_hash": document_hash,
            "document_url": document_url,
            "height": height,

        });

        #[cfg(feature = "nexa")]
        {
            tx_details
                .as_object_mut()
                .unwrap()
                .insert("txidem".to_string(), json!(tx.txidem().to_hex()));
        }

        Ok(tx_details)
    }

    fn get_balance(&self, scripthash: ScriptHash, timeout: &TimeoutTrigger) -> Result<Value> {
        let status = self
            .query
            .status(scripthash, OutputFlags::HasTokens, timeout)?;
        Ok(json!({
            "confirmed": status.confirmed_token_balance(),
            "unconfirmed": status.mempool_token_balance(self.query.confirmed_index()),
            "cursor": None::<String>,
        }))
    }

    fn get_scripthash_history(
        &self,
        scripthash: ScriptHash,
        token: Option<TokenID>,
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let status = self
            .query
            .status(scripthash, OutputFlags::HasTokens, timeout)?;
        let history = status.history(token);

        Ok(json!({
            "transactions": history,
            "cursor": None::<String>,
        }))
    }

    fn get_scripthash_mempool(
        &self,
        scripthash: ScriptHash,
        token: Option<TokenID>,
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        let status = self
            .query
            .status_mempool(scripthash, OutputFlags::HasTokens, timeout)?;
        Ok(json!({
            "transactions": status.history(token),
            "cursor": None::<String>,
        }))
    }

    fn listunspent(
        &self,
        scripthash: ScriptHash,
        token: Option<TokenID>,
        timeout: &TimeoutTrigger,
    ) -> Result<Value> {
        Ok(json!({
            "cursor": None::<String>,
            "unspent": self
                .query
                .status(scripthash, OutputFlags::HasTokens, timeout)?
                .unspent(token)
        }))
    }
}
