extern crate rostrum;

extern crate anyhow;
#[macro_use]
extern crate log;

use anyhow::Result;
use std::process;
use std::sync::Arc;

use rostrum::{
    app::App,
    cache::{BlockTxIDsCache, TransactionCache},
    chain::Chain,
    chaindef::Block,
    config::Config,
    daemon::Daemon,
    doslimit::{ConnectionLimits, GlobalLimits},
    index::Index,
    metrics::Metrics,
    query::Query,
    rpc::Rpc,
    signal::Waiter,
    store::{is_compatible_version, DBStore},
};

fn run_server(config: Arc<Config>) -> Result<()> {
    let signal = Waiter::start();
    let metrics = Arc::new(Metrics::new(config.monitoring_addr).expect("Failed to start metrics"));
    let blocktxids_cache = Arc::new(BlockTxIDsCache::new(
        config.blocktxids_cache_size as u64,
        &*metrics,
    ));

    let daemon = Arc::new(Daemon::new(
        &config.daemon_dir,
        config.daemon_rpc_addr,
        config.cookie_getter(),
        config.network_type,
        signal.clone(),
        blocktxids_cache,
        &*metrics,
    )?);
    // Perform initial indexing.
    if config.reindex {
        info!("Configuration flag `reindex` enabled. Running a full reindex.");
        DBStore::destroy(&config.db_path);
    }

    let compatible = is_compatible_version(&config.db_path, config.low_memory, &*metrics);

    if !compatible {
        info!("Incompatible database. Running full reindex.");
        DBStore::destroy(&config.db_path);
    }
    let store = DBStore::open(&config.db_path, config.low_memory, &*metrics).unwrap();
    let genesis: Block = daemon.get_genesis().expect("Failed to get genesis block");
    let index = Index::load(
        store,
        Chain::new(genesis),
        daemon.clone(),
        &*metrics,
        config.index_batch_size,
        config.reindex_last_blocks,
        config.cashaccount_activation_height,
    )?;
    index.update(&signal)?;

    let app = App::new(index, daemon, &config)?;
    let tx_cache = TransactionCache::new(config.tx_cache_size as u64, &*metrics);
    let query = Query::new(app.clone(), &*metrics, tx_cache, config.network_type)?;
    let connection_limits = ConnectionLimits::new(
        config.rpc_timeout,
        config.scripthash_subscription_limit,
        config.scripthash_alias_bytes_limit,
    );
    let global_limits = Arc::new(GlobalLimits::new(
        config.rpc_max_connections,
        config.rpc_max_connections_shared_prefix,
        &*metrics,
    ));

    let mut server: Option<Rpc> = None; // Electrum RPC server

    let rpc_addr = config.electrum_rpc_addr;
    let ws_addr = config.electrum_ws_addr;
    rostrum::thread::spawn("ws", move || {
        rostrum::wstcp::start_ws_proxy(ws_addr, rpc_addr);
        Ok(())
    });

    loop {
        let (headers_changed, new_tip) = app.update(&signal)?;
        let txs_changed = query.update_mempool()?;

        server = match server {
            Some(rpc) => {
                rpc.notify_scripthash_subscriptions(&headers_changed, txs_changed);
                if let Some(header) = new_tip {
                    rpc.notify_subscriptions_chaintip(header);
                }
                Some(rpc)
            }
            None => Some(Rpc::start(
                config.clone(),
                query.clone(),
                metrics.clone(),
                connection_limits,
                global_limits.clone(),
            )),
        };
        if let Err(err) = signal.wait(config.wait_duration) {
            info!("stopping server: {}", err);
            break;
        }
    }
    Ok(())
}

fn main() {
    let config = Arc::new(Config::from_args());
    if let Err(e) = run_server(config) {
        error!(
            "server failed: {}",
            e.chain().map(|x| x.to_string()).collect::<String>()
        );
        process::exit(1);
    }
}
