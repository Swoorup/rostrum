/// Benchmark full compaction.
extern crate rostrum;

#[macro_use]
extern crate log;

extern crate anyhow;

use rostrum::{config::Config, metrics::Metrics, store::DBStore};

use anyhow::Result;

fn run(config: Config) -> Result<()> {
    if !config.db_path.exists() {
        panic!(
            "DB {:?} must exist when running this benchmark!",
            config.db_path
        );
    }

    let metrics = Metrics::new(config.monitoring_addr).unwrap();

    let store = DBStore::open(&config.db_path, /*low_memory=*/ true, &metrics).unwrap();
    store.compact();
    Ok(())
}

fn main() {
    if let Err(e) = run(Config::from_args()) {
        error!("{}", e.chain().map(|x| x.to_string()).collect::<String>());
    }
}
