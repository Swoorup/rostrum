# See build documentation on how to build and run
# the docker file https://bitcoinunlimited.gitlab.io/rostrum/build/
FROM rust:1.64.0-slim-bullseye as builder

WORKDIR /build

RUN apt-get update \
    && apt-get install -y --no-install-recommends clang cmake build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY . .

ARG BUILD_FEATURES=""
RUN cargo install --features=$BUILD_FEATURES --locked --path .

# Create runtime image
FROM debian:bullseye-slim

WORKDIR /app

RUN apt-get update \
    && apt-get install -y --no-install-recommends curl \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN groupadd -r user \
    && adduser --disabled-login --system --shell /bin/false --uid 1000 --ingroup user user

COPY --from=builder --chown=user:user \
    /build/target/release/rostrum .

USER user

# Electrum RPC. Ports tcp and ws for mainnet and testnet on both BCH and NEXA.
# See https://bitcoinunlimited.gitlab.io/rostrum/ports/
EXPOSE 50001
EXPOSE 50003
EXPOSE 60001
EXPOSE 60003
EXPOSE 62001
EXPOSE 62003
EXPOSE 63001
EXPOSE 63003
EXPOSE 20001
EXPOSE 20003
EXPOSE 30001
EXPOSE 30003

# Prometheus monitoring
EXPOSE 4224
EXPOSE 14224
EXPOSE 3224
EXPOSE 13224

STOPSIGNAL SIGINT

HEALTHCHECK CMD curl -fSs http://localhost:4224/ || exit 1

ENTRYPOINT ["./rostrum"]
