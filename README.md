# Rostrum - Electrum Server in Rust

An efficient implementation of Electrum Server for Bitcoin Cash and Nexa with token support.

![logo](https://gitlab.com/bitcoinunlimited/rostrum/-/raw/master/contrib/images/logo-platform-256x256.png)

### See [Documentation](https://bitcoinunlimited.gitlab.io/rostrum/)
### See [Protocol Methods](https://bitcoinunlimited.gitlab.io/rostrum/protocol/methods)
### See [Downloads](https://bitcoinunlimited.gitlab.io/rostrum/install/)

## Project

Rostrum is an efficient implementation of Electrum Server and can be used
as a drop-in replacement for ElectrumX. In addition to the TCP RPC interface,
it also provides WebSocket support.

Rostrum fully implements the Electrum v1.4.3 protocol
plus many more queries, including full token support.

The server indexes the entire blockchain, and the resulting index
enables fast queries for blockchain applications and any given user wallet,
allowing the user to keep real-time track of his balances and his transaction
history.

When run on the user's own machine, there is no need for the wallet to
communicate with external Electrum servers,
thus preserving the privacy of the user's addresses and balances.

## Usage

See [here](https://bitcoinunlimited.gitlab.io/rostrum/usage/) for installation, build and usage instructions.

## Tests

Run unit tests with `cargo test`.

Rostrum has good integration test coverage with Nexa and Bitcoin Unlimited full node. See the [testing documentation](
https://bitcoinunlimited.gitlab.io/rostrum/tests/)

