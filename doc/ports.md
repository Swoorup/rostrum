# Default ports for Electrum

By default, Rostrum listens to clients on the following ports. SSL needs to
be [configured via reverse proxy](ssl.md).

| Protocol     | Bitcoin Cash  | Nexa  |
| ------------ | ------------- | ----- |
| Mainnet TCP  | 50001         | 20001 |
| Mainnet SSL  | 50002         | 20002 |
| Mainnet WS   | 50003         | 20003 |
| Mainnet WSS  | 50004         | 20004 |
| Testnet TCP  | 60001         | 30001 |
| Testnet SSL  | 60002         | 30002 |
| Testnet WS   | 60003         | 30003 |
| Testnet WSS  | 60004         | 30004 |
| Regtest TCP  | 60401         | 30401 |
| Testnet4 TCP | 62001         |       |
| Testnet4 SSL | 62002         |       |
| Testnet4 WS  | 62003         |       |
| Testnet4 WSS | 62004         |       |
| Scalenet TCP | 63001         |       |
| Scalenet SSL | 63002         |       |
| Scalenet WS  | 63003         |       |
| Scalenet WSS | 63004         |       |


## Prometheus monitoring

Default ports for [prometheus monitoring](monitoring.md).

| Protocol | Bitcoin Cash  | Nexa  |
| -------- | ------------- | ----- |
| Mainnet  | 4224          | 3224  |
| Testnet  | 14224         | 13224 |
| Regtest  | 24224         | 23224 |
| Testnet4 | 34224         |       |
| Scalenet | 44224         |       |
