# Protocol Methods

## blockchain.address.get\_balance

Return the confirmed and unconfirmed balances of a Bitcoin Cash address.

**Signature**

> Function:
> blockchain.address.get\_balance(address)
>
> Version added: 1.4.3
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Some server implementations may also support Legacy (base58)
>     addresses but are not required to do so by this specification.

**Result**

> See `blockchain.scripthash.get_balance`.


## blockchain.address.get\_first\_use

Returns a first occurance of usage of scripthash as ouput on the blockchain.

See `blockchain.scripthash.get_first_use`.

**Signature**

>  Function: blockchain.scripthash.get\_first\_use(address)
>
>  Version added: ROSTRUM ONLY. This function is a extension added to
>  rostrum and may not be available on other electrum server
>  implementations. Added in ElectrsCash 1.2
>
>  * *address*
>
>    The address as a Cash Address string (with or without prefix). Some server
>    implementations may also support Legacy (base58) addresses but are not
>    required to do so by this specification.

**Result**

> See `blockchain.scripthash.get_first_use`.

## blockchain.address.get\_history

Return the confirmed and unconfirmed history of a Bitcoin Cash address.

**Signature**

> Function:
> blockchain.address.get\_history(address)
>
> Version added: 1.4.3
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Some server implementations may also support Legacy (base58)
>     addresses but are not required to do so by this specification.

**Result**

> See `blockchain.scripthash.get_history`

## blockchain.address.get\_mempool

Return the unconfirmed transactions of a Bitcoin Cash address.

**Signature**

> Function:
> blockchain.address.get\_mempool(address)
>
> Version added:
> 1.4.3
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Some server implementations may also support Legacy (base58)
>     addresses but are not required to do so by this specification.

**Result**

> As for `blockchain.scripthash.get_mempool`

## blockchain.address.get\_scripthash

Translate a Bitcoin Cash or a Nexa address to a [script hash](basics.md). This method is
potentially useful for clients preferring to work with `script hashes` but
lacking the local libraries necessary to generate them.

**Signature**

> Function:
> blockchain.address.get\_scripthash(address)
>
> Version added:
> 1.4.3
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Some server implementations may also support Legacy (base58)
>     addresses but are not required to do so by this specification.

**Result**

> The unique 32-byte hex-encoded
> `script hash <script hashes>` that
> corresponds to the decoded address.

## blockchain.address.listunspent

Return an ordered list of UTXOs sent to a Bitcoin Cash address.

**Signature**

> Function:
> blockchain.address.listunspent(address)
>
> Version added:
> 1.4.3
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Some server implementations may also support Legacy (base58)
>     addresses but are not required to do so by this specification.

**Result**

> As for `blockchain.scripthash.listunspent`

## blockchain.address.subscribe

Subscribe to a Bitcoin Cash address.

**Signature**

> Function:
> blockchain.address.subscribe(address)
>
> Version added:
> 1.4.3
>
> *address*
>
> > The address as a Cash Address string (with or without prefix). Some
> > server implementations may also support Legacy (base58) addresses
> > but are not required to do so by this specification.

**Result**

> The `status <status>` of the address.

**Notifications**

> As this is a subcription, the client will receive a notification when
> the `status <status>` of the address
> changes. Its signature is
>
> Function:
> blockchain.address.subscribe(address, status)
>
|Note|
|----|
| The address returned back to the client when notifying of status changes will be in the same encoding and syle as was provided when subscribing. In effect, a whitespace-stripped version of the address string that the client provided will be sent back to the client when notifying, in order to make it easier for clients to track the notification. |
| It is unspecified what happens if a client subscribes to the same address using multiple encodings or styles, but it is RECOMMENDED that servers simply update their internal subscription tables on subsequent subscriptions to the same destination such that they honor the latest subscription only, and not subscribe clients multiple times to the same logical destination. For example, Rostrum server will simply update its table for how to refer to the subscription and send clients subsequent notifications using the latest encoding style of that particular address that the client last provided. |
| Similarly, if a client mixes `blockchain.address.*` and `blockchain.scripthash.*` calls to the server, it is RECOMMENDED that the server treat all addresses as equivalent to their scripthashes internally such that it is possible to subscribe by address and later unsubscribe by scripthash, for example. |

## blockchain.address.unsubscribe

Unsubscribe from a Bitcoin Cash address, preventing future notifications
if its `status <status>` changes.

**Signature**

> Function:
> blockchain.address.unsubscribe(address)
>
> Version added:
> 1.4.3
>
> *address*
>
> > The address as a Cash Address string (with or without prefix). Some
> > server implementations may also support Legacy (base58) addresses
> > but are not required to do so by this specification.

**Result**

> Returns `True` if the address was
> subscribed to, otherwise `False`. Note
> that `False` might be returned even
> for something subscribed to earlier, because the server can drop
> subscriptions in rare circumstances.

## blockchain.block.header

Return the block header at the given height.

**Signature**

> Function:
> blockchain.block.header(height, cp\_height=0)
>
> Version added:
> 1.3
>
> Version changed:
> 1.4 *cp_height* parameter added
>
> Version changed:
> 1.4.1
>
> *height*
>
> > The height of the block, a non-negative integer.
>
> *cp_height*
>
> > Checkpoint height, a non-negative integer. Ignored if zero,
> > otherwise the following must hold:
> >
> > > *height* <= *cp_height*

**Result**

> If *cp_height* is zero, the raw block header as a hexadecimal string.
>
> Otherwise a dictionary with the following keys. This provides a proof
> that the given header is present in the blockchain; presumably the
> client has the merkle root hard-coded as a checkpoint.
>
> -   *branch*
>
>     The merkle branch of *header* up to *root*, deepest pairing first.
>
> -   *header*
>
>     The raw block header as a hexadecimal string. Starting with
>     version 1.4.1, AuxPoW data (if present in the original header) is
>     truncated.
>
> -   *root*
>
>     The merkle root of all blockchain headers up to and including
>     *cp_height*.

**Example Result**

With *height* 5 and *cp_height* 0 on the Bitcoin Cash chain:

    "0100000085144a84488ea88d221c8bd6c059da090e88f8a2c99690ee55dbba4e00000000e11c48fecdd9e72510ca84f023370c9a38bf91ac5cae88019bee94d24528526344c36649ffff001d1d03e477"

With *cp_height* 8:

    {
      "branch": [
         "000000004ebadb55ee9096c9a2f8880e09da59c0d68b1c228da88e48844a1485",
         "96cbbc84783888e4cc971ae8acf86dd3c1a419370336bb3c634c97695a8c5ac9",
         "965ac94082cebbcffe458075651e9cc33ce703ab0115c72d9e8b1a9906b2b636",
         "89e5daa6950b895190716dd26054432b564ccdc2868188ba1da76de8e1dc7591"
         ],
      "header": "0100000085144a84488ea88d221c8bd6c059da090e88f8a2c99690ee55dbba4e00000000e11c48fecdd9e72510ca84f023370c9a38bf91ac5cae88019bee94d24528526344c36649ffff001d1d03e477",
      "root": "e347b1c43fd9b5415bf0d92708db8284b78daf4d0e24f9c3405f45feb85e25db"
    }

## blockchain.block.headers

Return a concatenated chunk of block headers from the main chain.

**Signature**

> Function:
> blockchain.block.headers(start\_height, count, cp\_height=0)
>
> Version added:
> 1.2
>
> Version changed:
> 1.4 *cp_height* parameter added
>
> Version changed:
> 1.4.1
>
> *start_height*
>
> > The height of the first header requested, a non-negative integer.
>
> *count*
>
> > The number of headers requested, a non-negative integer.
>
> *cp_height*
>
> > Checkpoint height, a non-negative integer. Ignored if zero,
> > otherwise the following must hold:
> >
> > > *start_height* + (*count* - 1) <= *cp_height*

**Result**

> A dictionary with the following members:
>
> -   *count*
>
>     The number of headers returned, between zero and the number
>     requested. If the chain has not extended sufficiently far, only
>     the available headers will be returned. If more headers than *max*
>     were requested at most *max* will be returned.
>
> -   *hex*
>
>     The binary block headers concatenated together in-order as a
>     hexadecimal string. Starting with version 1.4.1, AuxPoW data (if
>     present in the original header) is truncated if *cp_height* is
>     nonzero.
>
> -   *max*
>
>     The maximum number of headers the server will return in a single
>     request.
>
> The dictionary additionally has the following keys if *count* and
> *cp_height* are not zero. This provides a proof that all the given
> headers are present in the blockchain; presumably the client has the
> merkle root hard-coded as a checkpoint.
>
> -   *root*
>
>     The merkle root of all blockchain headers up to and including
>     *cp_height*.
>
> -   *branch*
>
>     The merkle branch of the last returned header up to *root*,
>     deepest pairing first.

**Example Response**

See `here <cp_height example>` for an
example of *root* and *branch* keys.

    {
      "count": 2,
      "hex": "0100000000000000000000000000000000000000000000000000000000000000000000003ba3edfd7a7b12b27ac72c3e67768f617fc81bc3888a51323a9fb8aa4b1e5e4a29ab5f49ffff001d1dac2b7c010000006fe28c0ab6f1b372c1a6a246ae63f74f931e8365e15a089c68d6190000000000982051fd1e4ba744bbbe680e1fee14677ba1a3c3540bf7b1cdb606e857233e0e61bc6649ffff001d01e36299"
      "max": 2016
    }

## blockchain.estimatefee

Return the estimated transaction fee per kilobyte for a transaction to
be confirmed within a certain number of blocks.

**Signature**

> Function:
> blockchain.estimatefee(number)
> :::
>
> *number*
>
> > The number of blocks to target for confirmation.

**Result**

> The estimated transaction fee in coin units per kilobyte, as a
> floating point number. If the daemon does not have enough information
> to make an estimate, the integer `-1` is returned.

**Example Result**

    0.00101079

## blockchain.headers.subscribe

Subscribe to receive block headers when a new block is found.

**Signature**

> Function:
> blockchain.headers.subscribe()

**Result**

> The header of the current block chain tip. The result is a dictionary
> with two members:
>
> -   *hex*
>
>     The binary header as a hexadecimal string.
>
> -   *height*
>
>     The height of the header, an integer.

**Example Result**

    {
      "height": 520481,
      "hex": "00000020890208a0ae3a3892aa047c5468725846577cfcd9b512b50000000000000000005dc2b02f2d297a9064ee103036c14d678f9afc7e3d9409cf53fd58b82e938e8ecbeca05a2d2103188ce804c4"
    }

**Notifications**

> As this is a subcription, the client will receive a notification when
> a new block is found. The notification's signature is:
>
> > Function:
> > blockchain.headers.subscribe(header)
> >
> > -   *header*
> >
> >     See **Result** above.

|Note|
|----|
|Should a new block arrive quickly, perhaps while the server is still processing prior blocks, the server may only notify of the most recent chain tip. The protocol does not guarantee notification of all intermediate block headers.|
|In a similar way the client must be prepared to handle chain reorganisations. Should a re-org happen the new chain tip will not sit directly on top of the prior chain tip. The client must be able to figure out the common ancestor block and request any missing block headers to acquire a consistent view of the chain state.|
|If a client subscribes multiple times, the subsequent subscriptions will be ignored and the client will continue to receive one notification per header.|

## blockchain.relayfee

Return the minimum fee a low-priority transaction must pay in order to
be accepted to the daemon's memory pool.

**Signature**

> Function:
> blockchain.relayfee()

**Result**

> The fee in whole coin units (BTC, not satoshis for Bitcoin) as a
> floating point number.

**Example Results**

    1e-05

    0.0

## blockchain.scripthash.get\_balance

Return the confirmed and unconfirmed balances of a `script hash`.

**Signature**

> Function:
> blockchain.scripthash.get\_balance(scripthash)
>
> Version added:
> 1.1
>
> *scripthash*
>
> > The script hash as a hexadecimal string.

**Result**

> A dictionary with keys `confirmed` and
> `unconfirmed`. The value of each is the appropriate
> balance in satoshis.

**Result Example**

    {
      "confirmed": 103873966,
      "unconfirmed": 236844
    }

## blockchain.scripthash.get\_first\_use

Returns a first occurance of usage of scripthash as ouput on the blockchain. Any
transaction in a confirmed block may be returned, as long as it's the first
block that contains use of scripthash.

If there are no matching transactions in the blockchain, but there are in the
mempool, one of these are returned instead. In this case, the block hash
returned is the hex representation of `0` and block height the numeric value
`0`

If there are no matching transactions in the blockchain or on the mempool, an
error is returned.

If the server supports this method if it has the key `firstuse` with
value `["1.0"]` in its `server.features` response.

Rationale for requiring the scripthash being an output is to allow wallets to
verify the response without having to fetch the input transactions. There
cannot exist earlier occurances of scripthash being used as input, an ouput
must exist first.

**Signature**

>  Function: blockchain.block.get\_first\_use(scripthash)
>  Version added: ROSTRUM ONLY. This function is a extension added to
>  rostrum and may not be available on other electrum server
>  implementations. Added in ElectrsCash 1.2
>
>  *scripthash*
>
>    The script hash as a hexadecimal string.

**Result**

>  A dictionary with the following keys.
>
>  * *height*
>
>    The block height of the block where the first occurance of scripthash
>    exists.
>
>    Block height is 0 if the first occurance is in the mempool.
>
>  * *block_hash*
>
>    The block hash of the block where the first occurance of scripthash exists.
>
>    Block hash is hex of 0 if the first occurance is in the mempool.
>
>  * *tx_hash*
>
>   A hash of single transaction containing the scripthash in its output.

**Example Result**

With *scripthash*
`273139c9327743dccc1030fc8d009e63e41e147c6b0449913d2dea5b71eef230` on the
Bitcoin Cash chain:

```
 {
    'block_hash': '000000000000000002a04f56505ef459e1edd21fb3725524116fdaedf3a4d0ab',
    'block_height': 597843,
    'tx_hash': 'cef9c1485f166e92adf0b5d42e4ad202e4f5f9e2e7a072d0a6864c61c2cd6fa0'
  }
```

## blockchain.scripthash.get\_history

Return the confirmed and unconfirmed history of a `script hash <script hashes>`

**Signature**

> Function:
> blockchain.scripthash.get\_history(scripthash)
>
> Version added:
> 1.1
>
> *scripthash*
>
> > The script hash as a hexadecimal string.

**Result**

> A list of confirmed transactions in blockchain order, with the output
> of `blockchain.scripthash.get_mempool`
> appended to the list. Each confirmed transaction is a dictionary with
> the following keys:
>
> -   *height*
>
>     The integer height of the block the transaction was confirmed in.
>
> -   *tx_hash*
>
>     The transaction hash in hexadecimal.
>
> See `blockchain.scripthash.get_mempool`
> for how mempool transactions are returned.

**Result Examples**

    [
      {
        "height": 200004,
        "tx_hash": "acc3758bd2a26f869fcc67d48ff30b96464d476bca82c1cd6656e7d506816412"
      },
      {
        "height": 215008,
        "tx_hash": "f3e1bf48975b8d6060a9de8884296abb80be618dc00ae3cb2f6cee3085e09403"
      }
    ]

    [
      {
        "fee": 20000,
        "height": 0,
        "tx_hash": "9fbed79a1e970343fcd39f4a2d830a6bde6de0754ed2da70f489d0303ed558ec"
      }
    ]

## blockchain.scripthash.get\_mempool

Return the unconfirmed transactions of a `script hash <script hashes>`.

**Signature**

> Function:
> blockchain.scripthash.get\_mempool(scripthash)
>
> Version added:
> 1.1
>
> *scripthash*
>
> > The script hash as a hexadecimal string.

**Result**

> A list of mempool transactions in arbitrary order. Each mempool
> transaction is a dictionary with the following keys:
>
> -   *height*
>
>     `0` if all inputs are confirmed, and `-1` otherwise.
>
> -   *tx_hash*
>
>     The transaction hash in hexadecimal.
>
> -   *fee*
>
>     The transaction fee in minimum coin units (satoshis).

**Result Example**

    [
      {
        "tx_hash": "45381031132c57b2ff1cbe8d8d3920cf9ed25efd9a0beb764bdb2f24c7d1c7e3",
        "height": 0,
        "fee": 24310
      }
    ]

## blockchain.scripthash.listunspent

Return an ordered list of UTXOs sent to a script hash.

**Signature**

> Function:
> blockchain.scripthash.listunspent(scripthash)
>
> Version added:
> 1.1
>
> *scripthash*
>
> > The script hash as a hexadecimal string.

**Result**

> A list of unspent outputs in blockchain order. This function takes the
> mempool into account. Mempool transactions paying to the address are
> included at the end of the list in an undefined order. Any output that
> is spent in the mempool does not appear. Each output is a dictionary
> with the following keys:
>
> -   *height*
>
>     The integer height of the block the transaction was confirmed in.
>     `0` if the transaction is in the mempool.
>
> -   *tx_pos*
>
>     The zero-based index of the output in the transaction's list of
>     outputs.
>
> -   *tx_hash*
>
>     The output's transaction hash as a hexadecimal string. The hash
>     is little-endian encoded (same as bitcoind RPC).
>
> -   *value*
>
>     The output's value in minimum coin units (satoshis).

**Result Example**

    [
      {
        "tx_pos": 0,
        "value": 45318048,
        "tx_hash": "9f2c45a12db0144909b5db269415f7319179105982ac70ed80d76ea79d923ebf",
        "height": 437146
      },
      {
        "tx_pos": 0,
        "value": 919195,
        "tx_hash": "3d2290c93436a3e964cfc2f0950174d8847b1fbe3946432c4784e168da0f019f",
        "height": 441696
      }
    ]

## blockchain.scripthash.subscribe

Subscribe to a script hash.

**Signature**

> Function:
> blockchain.scripthash.subscribe(scripthash)
>
> Version added:
> 1.1
>
> *scripthash*
>
> > The script hash as a hexadecimal string.

**Result**

> The `status <status>` of the script
> hash.

**Notifications**

> The client will receive a notification when the
> `status <status>` of the script hash
> changes. Its signature is
>
> > Function:
> > blockchain.scripthash.subscribe(scripthash, status)

## blockchain.scripthash.unsubscribe

Unsubscribe from a script hash, preventing future notifications if its
`status
<status>` changes.

**Signature**

> Function:
> blockchain.scripthash.unsubscribe(scripthash)
>
> Version added:
> 1.4.2
>
> *scripthash*
>
> > The script hash as a hexadecimal string.

**Result**

> Returns `True` if the scripthash was
> subscribed to, otherwise `False`. Note
> that `False` might be returned even
> for something subscribed to earlier, because the server can drop
> subscriptions in rare circumstances.

## blockchain.transaction.broadcast

Broadcast a transaction to the network.

**Signature**

> Function:
> blockchain.transaction.broadcast(raw\_tx)
>
> Version changed:
> 1.1 errors returned as JSON RPC errors rather than as a result.
>
> *raw_tx*
>
> > The raw transaction as a hexadecimal string.

**Result**

> The transaction hash as a hexadecimal string.
>
> **Note** protocol version 1.0 (only) does not respond according to the
> JSON RPC specification if an error occurs. If the daemon rejects the
> transaction, the result is the error message string from the daemon,
> as if the call were successful. The client needs to determine if an
> error occurred by comparing the result to the expected transaction
> hash.

**Result Examples**

    "a76242fce5753b4212f903ff33ac6fe66f2780f34bdb4b33b175a7815a11a98e"

Protocol version 1.0 returning an error as the result:

    "258: txn-mempool-conflict"

## blockchain.transaction.get

Return a raw transaction.

**Signature**

> Function:
> blockchain.transaction.get(tx\_hash, verbose=false)
>
> Version changed:
> 1.1 ignored argument *height* removed
>
> Version changed:
> 1.2 *verbose* argument added
>
> *tx_hash*
>
> > The transaction hash as a hexadecimal string.
>
> *verbose*
>
> > Whether a verbose coin-specific response is required.

**Result**

> If *verbose* is `false`:
>
> > The raw transaction as a hexadecimal string.
>
> If *verbose* is `true`:
>
> > The result is a coin-specific dictionary -- whatever the coin
> > daemon returns when asked for a verbose form of the raw transaction.
>
> If transaction does not exist on blockhain or in the mempool, an error
> is returned.

**Example Results**

When *verbose* is `false`:

    "01000000015bb9142c960a838329694d3fe9ba08c2a6421c5158d8f7044cb7c48006c1b48"
    "4000000006a4730440220229ea5359a63c2b83a713fcc20d8c41b20d48fe639a639d2a824"
    "6a137f29d0fc02201de12de9c056912a4e581a62d12fb5f43ee6c08ed0238c32a1ee76921"
    "3ca8b8b412103bcf9a004f1f7a9a8d8acce7b51c983233d107329ff7c4fb53e44c855dbe1"
    "f6a4feffffff02c6b68200000000001976a9141041fb024bd7a1338ef1959026bbba86006"
    "4fe5f88ac50a8cf00000000001976a91445dac110239a7a3814535c15858b939211f85298"
    "88ac61ee0700"

When *verbose* is `true`:

    {
      "blockhash": "0000000000000000015a4f37ece911e5e3549f988e855548ce7494a0a08b2ad6",
      "blocktime": 1520074861,
      "confirmations": 679,
      "hash": "36a3692a41a8ac60b73f7f41ee23f5c917413e5b2fad9e44b34865bd0d601a3d",
      "hex": "01000000015bb9142c960a838329694d3fe9ba08c2a6421c5158d8f7044cb7c48006c1b484000000006a4730440220229ea5359a63c2b83a713fcc20d8c41b20d48fe639a639d2a8246a137f29d0fc02201de12de9c056912a4e581a62d12fb5f43ee6c08ed0238c32a1ee769213ca8b8b412103bcf9a004f1f7a9a8d8acce7b51c983233d107329ff7c4fb53e44c855dbe1f6a4feffffff02c6b68200000000001976a9141041fb024bd7a1338ef1959026bbba860064fe5f88ac50a8cf00000000001976a91445dac110239a7a3814535c15858b939211f8529888ac61ee0700",
      "locktime": 519777,
      "size": 225,
      "time": 1520074861,
      "txid": "36a3692a41a8ac60b73f7f41ee23f5c917413e5b2fad9e44b34865bd0d601a3d",
      "version": 1,
      "vin": [ {
        "scriptSig": {
          "asm": "30440220229ea5359a63c2b83a713fcc20d8c41b20d48fe639a639d2a8246a137f29d0fc02201de12de9c056912a4e581a62d12fb5f43ee6c08ed0238c32a1ee769213ca8b8b[ALL|FORKID] 03bcf9a004f1f7a9a8d8acce7b51c983233d107329ff7c4fb53e44c855dbe1f6a4",
          "hex": "4730440220229ea5359a63c2b83a713fcc20d8c41b20d48fe639a639d2a8246a137f29d0fc02201de12de9c056912a4e581a62d12fb5f43ee6c08ed0238c32a1ee769213ca8b8b412103bcf9a004f1f7a9a8d8acce7b51c983233d107329ff7c4fb53e44c855dbe1f6a4"
        },
        "sequence": 4294967294,
        "txid": "84b4c10680c4b74c04f7d858511c42a6c208bae93f4d692983830a962c14b95b",
        "vout": 0}],
      "vout": [ { "n": 0,
                 "scriptPubKey": { "addresses": [ "12UxrUZ6tyTLoR1rT1N4nuCgS9DDURTJgP"],
                                   "asm": "OP_DUP OP_HASH160 1041fb024bd7a1338ef1959026bbba860064fe5f OP_EQUALVERIFY OP_CHECKSIG",
                                   "hex": "76a9141041fb024bd7a1338ef1959026bbba860064fe5f88ac",
                                   "reqSigs": 1,
                                   "type": "pubkeyhash"},
                 "value": 0.0856647},
               { "n": 1,
                 "scriptPubKey": { "addresses": [ "17NMgYPrguizvpJmB1Sz62ZHeeFydBYbZJ"],
                                   "asm": "OP_DUP OP_HASH160 45dac110239a7a3814535c15858b939211f85298 OP_EQUALVERIFY OP_CHECKSIG",
                                   "hex": "76a91445dac110239a7a3814535c15858b939211f8529888ac",
                                   "reqSigs": 1,
                                   "type": "pubkeyhash"},
                 "value": 0.1360904}]}

## blockchain.transaction.get\_confirmed\_blockhash

Returns the blockhash of a block the transaction confirmed in. Returns error
if transaction is not confirmed (or does not exist).

**Signature**

> Function: blockchain.transaction.get\_confirmed\_blockhash(txid)`
>
>  Version added: ROSTRUM ONLY. This function is a extension added to
>  rostrum and may not be available on other electrum server
>  implementations.
>
> * `txid` - Transaction ID

> A dictionary with the following keys:
>
> -   *block_hash*
>
>     The hash of the block in which the transaction was confirmed.
>
> -   *block_height*
>
>     The height of the block in which the transaction was confirmed.


**Example result**
```
{
'block_hash': '000000000000000002a04f56505ef459e1edd21fb3725524116fdaedf3a4d0ab',
'block_height': 597843,
}
```

## blockchain.transaction.get\_merkle

Return the merkle branch to a confirmed transaction given its hash and
height.

**Signature**

> Function:
> blockchain.transaction.get\_merkle(tx\_hash, height *(optional)*)
>
> *tx_hash*
>
> > The transaction hash as a hexadecimal string.
>
> *height*
>
> > The height at which it was confirmed, an integer.
> > This argument is optional (ROSTRUM ONLY)

**Result**

> A dictionary with the following keys:
>
> -   *block_height*
>
>     The height of the block the transaction was confirmed in.
>
> -   *merkle*
>
>     A list of transaction hashes the current hash is paired with,
>     recursively, in order to trace up to obtain merkle root of the
>     block, deepest pairing first.
>
> -   *pos*
>
>     The 0-based index of the position of the transaction in the
>     ordered list of transactions in the block.

**Result Example**

    {
      "merkle":
      [
        "713d6c7e6ce7bbea708d61162231eaa8ecb31c4c5dd84f81c20409a90069cb24",
        "03dbaec78d4a52fbaf3c7aa5d3fccd9d8654f323940716ddf5ee2e4bda458fde",
        "e670224b23f156c27993ac3071940c0ff865b812e21e0a162fe7a005d6e57851",
        "369a1619a67c3108a8850118602e3669455c70cdcdb89248b64cc6325575b885",
        "4756688678644dcb27d62931f04013254a62aeee5dec139d1aac9f7b1f318112",
        "7b97e73abc043836fd890555bfce54757d387943a6860e5450525e8e9ab46be5",
        "61505055e8b639b7c64fd58bce6fc5c2378b92e025a02583303f69930091b1c3",
        "27a654ff1895385ac14a574a0415d3bbba9ec23a8774f22ec20d53dd0b5386ff",
        "5312ed87933075e60a9511857d23d460a085f3b6e9e5e565ad2443d223cfccdc",
        "94f60b14a9f106440a197054936e6fb92abbd69d6059b38fdf79b33fc864fca0",
        "2d64851151550e8c4d337f335ee28874401d55b358a66f1bafab2c3e9f48773d"
      ],
      "block_height": 450538,
      "pos": 710
    }

## blockchain.transaction.id\_from\_pos

Return a transaction hash and optionally a merkle proof, given a block
height and a position in the block.

**Signature**

> Function:
> blockchain.transaction.id\_from\_pos(height, tx\_pos, merkle=false)
>
> Version added:
> 1.4
>
> *height*
>
> > The main chain block height, a non-negative integer.
>
> *tx_pos*
>
> > A zero-based index of the transaction in the given block, an
> > integer.
>
> *merkle*
>
> > Whether a merkle proof should also be returned, a boolean.

**Result**

> If *merkle* is `false`, the
> transaction hash as a hexadecimal string. If `true`, a dictionary with the following keys:
>
> -   *tx_hash*
>
>     The transaction hash as a hexadecimal string.
>
> -   *merkle*
>
>     A list of transaction hashes the current hash is paired with,
>     recursively, in order to trace up to obtain merkle root of the
>     block, deepest pairing first.

**Example Results**

When *merkle* is `false`:

    "fc12dfcb4723715a456c6984e298e00c479706067da81be969e8085544b0ba08"

When *merkle* is `true`:

    {
      "tx_hash": "fc12dfcb4723715a456c6984e298e00c479706067da81be969e8085544b0ba08",
      "merkle":
      [
        "928c4275dfd6270349e76aa5a49b355eefeb9e31ffbe95dd75fed81d219a23f8",
        "5f35bfb3d5ef2ba19e105dcd976928e675945b9b82d98a93d71cbad0e714d04e",
        "f136bcffeeed8844d54f90fc3ce79ce827cd8f019cf1d18470f72e4680f99207",
        "6539b8ab33cedf98c31d4e5addfe40995ff96c4ea5257620dfbf86b34ce005ab",
        "7ecc598708186b0b5bd10404f5aeb8a1a35fd91d1febbb2aac2d018954885b1e",
        "a263aae6c470b9cde03b90675998ff6116f3132163911fafbeeb7843095d3b41",
        "c203983baffe527edb4da836bc46e3607b9a36fa2c6cb60c1027f0964d971b29",
        "306d89790df94c4632d652d142207f53746729a7809caa1c294b895a76ce34a9",
        "c0b4eff21eea5e7974fe93c62b5aab51ed8f8d3adad4583c7a84a98f9e428f04",
        "f0bd9d2d4c4cf00a1dd7ab3b48bbbb4218477313591284dcc2d7ca0aaa444e8d",
        "503d3349648b985c1b571f59059e4da55a57b0163b08cc50379d73be80c4c8f3"
      ]
    }

## blockchain.utxo.get

Returns data on a specified output of specific transaction. Returns error
if transaction or output does not exist.

If the output is spent, information about the spender is provided. This allows
a SPV client to call `blockchain.transaction.get\_merkle` to generate a merkle
branch, proving that it is spent.

**Signature**

>  Function BCH: `blockchain.utxo.get(tx_hash, output_index)`
>
>  Function NXA: `blockchain.utxo.get(outpoint_hash)`
>
>  Version added: ROSTRUM ONLY. This function is a extension added to
>  rostrum and may not be available on other electrum server
>  implementations.
>
> *tx_hash*
>
> > The transaction ID as hexadecimal string.
>
> *output\_index*
>
> > The vout position in the transaction.
>
> *outpoint\_hash*
>
> > Hash of utxo (hash of transaction idem + output index)

**Result**

> A dictionary with the following keys:
>
> -  *state*
>
>    State of the utxo. A string that is "spent" or "unspent".
>
> -  *height*
>
>    The height the utxo was confirmed in. If it is unconfirmed, the
>    value is 0 if all inputs are confirmed, and -1 otherwise.
>
> -  *value*
>
>    The output’s value in minimum coin units (satoshis).
>
> -  *scripthash*
>
>    The scriphash of the output scriptPubKey.
>
> -  *spent*
>
>    The transaction spending the utxo with the following keys:
>
> -  *tx\_pos*
>
>    The zero-based index of the input in the transaction’s list of inputs. Null if utxo is unspent.
>
> -  *tx_hash*
>
>    The transaction ID. Null if utxo is unspent.
>
> -  *height*
>
>    The height the transaction was confirmed in. If it is unconfirmed, the
>    value is 0 if all inputs are confirmed, and -1 otherwise. Null if utxo is unspent.

**Example Results**
```
{
    "amount": 4999999000,
    "height": 100000,
    "scripthash": "2e6d15f1a36288b55d5cb14d21f00324cbf767b459dc37e5054e383e434e0b16",
    "spent": {
        "height": -1,
        "tx_hash": "90adba10cdb91546b9c17e93ee300fe7940c6c3dda80f83bb791df5895d83aff",
        "tx_pos": 0
    },
    "status": "spent"
},
```

## cashaccount.query.name

BCH ONLY.

Requires that it has been enabled in rostrum with the `cashaccount_activation_height`
parameter.

Returns the transactions registering a cashaccount at blockheight. Note that
height is absolute blockheight and you need to add the cashaccount block
modification value yourself.

The cashaccount block modification value for Bitcoin Cash is 563620.

For example, to lookup dagur#216, call `blockchain.query.name("dagur", 216 +
563620)`

**Signature**

> Function BCH: `cashaccount.query.name(name, height)`
>
>  Version added: ROSTRUM ONLY. This function is a extension added to
>  rostrum and may not be available on other electrum server
>  implementations.
>
> *name*
>
> > Cash account name
>
> *height*
>
> > Block height for registration (without cashaccount offset subtracted)

**Result**

> A dictionary with the following keys:
>
> -  *blockhash*
>
>    Block hash of the block that the cash account was confirmed in.
>
> -  *height*
>
>    Block height of the block that the cash account was confirmed in.
>
> -  *tx*
>
>    A hex string of the transaction containing the cash account.

**Example Results**

For query `blockchain.query.name('dagur', 563836)`

```
{
    'blockhash': '000000000000000003c73e50b9de6317c4d2b2ac5f3c1253b01e61a6e329219a',
    'height': 563836,
    'tx': '0100000001bca903bbc429218234857628b382e8aa8e3bfa74c5b59628ad053284e50bf6ac010000006b4830450221009bbd0a96ef5ef33e09c4fce7fafd2add714ebe05d87a9cb6c826b863d0e99225022039d77b8bd9c8067636e64d6f1aeeeeb8b816bbc875afd04cef9eb299df83b7d64121037a291b1a7f21b03b2a5120434b7a06b61944e0edc1337c76d737d0b5fa1c871fffffffff020000000000000000226a040101010105646167757215018c092ec2cbd842e89432c7c53b54db3a958c83a575f00d00000000001976a914dfdd3e914d73fee85ad40cd71430327f0404c15488ac00000000'
}
```

## mempool.get\_fee\_histogram

Return a histogram of the fee rates paid by transactions in the memory
pool, weighted by transaction size.

**Signature**

> Function:
> mempool.get\_fee\_histogram()
>
> Version added:
> 1.2

**Result**

> The histogram is an array of \[*fee*, *vsize*\] pairs, where is the
> cumulative virtual size of mempool transactions with a fee rate in the
> interval \[, \], and \> .
>
> Fee intervals may have variable size. The choice of appropriate
> intervals is currently not part of the protocol.

**Example Result**

>     [[12, 128812], [4, 92524], [2, 6478638], [1, 22890421]]

## server.add\_peer

A newly-started server uses this call to get itself into other servers\'
peers lists. It sould not be used by wallet clients.

**Signature**

> Function:
> server.add\_peer(features)
>
> Version added:
> 1.1
>
> -   *features*
>
>     The same information that a call to the sender\'s
>     `server.features`{.interpreted-text role="func"} RPC call would
>     return.

**Result**

> A boolean indicating whether the request was tentatively accepted. The
> requesting server will appear in
> `server.peers.subscribe` when further
> sanity checks complete successfully.

## server.banner

Return a banner to be shown in the Electrum console.

**Signature**

> Function:
> server.banner()

**Result**

> A string.

**Example Result**

>     "Welcome to Electrum!"

## server.donation\_address

Return a server donation address.

**Signature**

> Function:
> server.donation\_address()

**Result**

> A string.

**Example Result**

>     "1BWwXJH3q6PRsizBkSGm2Uw4Sz1urZ5sCj"

## server.features

Return a list of features and services supported by the server.

**Signature**

> Function:
> server.features()
>
> Version changed:
> 1.4.2 *hosts* key is no longer required, but recommended.

**Result**

> A dictionary of keys and values. Each key represents a feature or
> service of the server, and the value gives additional information.
>
> The following features MUST be reported by the server. Additional
> key-value pairs may be returned.
>
> -   *genesis_hash*
>
>     The hash of the genesis block. This is used to detect if a peer is
>     connected to one serving a different network.
>
> -   *hash_function*
>
>     The hash function the server uses for `script hashing
>     <script hashes>`{.interpreted-text role="ref"}. The client must
>     use this function to hash pay-to-scripts to produce script hashes
>     to send to the server. The default is "sha256". "sha256" is
>     currently the only acceptable value.
>
> -   *server_version*
>
>     A string that identifies the server software. Should be the same
>     as the result to the `server.version` RPC call.
>
> -   *protocol_max*
>
> -   *protocol_min*
>
>     Strings that are the minimum and maximum Electrum protocol
>     versions this server speaks. Example: "1.1".
>
> -   *pruning*
>
>     An integer, the pruning limit. Omit or set to
>     `null` if there is no pruning
>     limit. Should be the same as what would suffix the letter `p` in
>     the IRC real name.
>
> The following features are RECOMMENDED that be reported by the
> servers.
>
> -   *hosts*
>
>     A dictionary, keyed by host name, that this server can be reached
>     at. If this dictionary is missing, then this is a way to signal to
>     other servers that while this host is reachable, it does not wish
>     to peer with other servers. A server SHOULD stop peering with a
>     peer if it sees the *hosts* dictionary for its peer is empty
>     and/or no longer contains the expected route (e.g. hostname).
>     Normally this dictionary will only contain a single entry; other
>     entries can be used in case there are other connection routes
>     (e.g. Tor).
>
>     The value for a host is itself a dictionary, with the following
>     optional keys:
>
>     -   *ssl_port*
>
>         An integer. Omit or set to `null`
>          if SSL connectivity is not provided.
>
>     -   *tcp_port*
>
>         An integer. Omit or set to `null`
>         if TCP connectivity is not provided.
>
>     -   *ws_port*
>
>         An integer. Omit or set to `null`
>         if Web Socket (<ws://>) connectivity is not
>         provided.
>
>     -   *wss_port*
>
>         An integer. Omit or set to `null`
>         if Web Socket Secure (<wss://>) connectivity is
>         not provided.
>
>     A server should ignore information provided about any host other
>     than the one it connected to.

**Example Result**

    {
        "genesis_hash": "000000000933ea01ad0ee984209779baaec3ced90fa3f408719526f8d77f4943",
        "hosts": {"14.3.140.101": {"tcp_port": 51001, "ssl_port": 51002}},
        "protocol_max": "1.0",
        "protocol_min": "1.0",
        "pruning": null,
        "server_version": "ElectrumX 1.0.17",
        "hash_function": "sha256"
    }

## server.peers.subscribe

Return a list of peer servers. Despite the name this is not a
subscription and the server must send no notifications.

**Signature**

> Function:
> server.peers.subscribe()

**Result**

> An array of peer servers, each returned as a 3-element array. For
> example:
>
>     ["107.150.45.210",
>      "e.anonyhost.org",
>      ["v1.0", "p10000", "t", "s995"]]
>
> The first element is the IP address, the second is the host name
> (which might also be an IP address), and the third is a list of server
> features. Each feature and starts with a letter. \'v\' indicates the
> server maximum protocol version, \'p\' its pruning limit and is
> omitted if it does not prune, \'t\' is the TCP port number, and \'s\'
> is the SSL port number. If a port is not given for \'s\' or \'t\' the
> default port for the coin network is implied. If \'s\' or \'t\' is
> missing then the server does not support that transport.

## server.ping

Ping the server to ensure it is responding, and to keep the session
alive. The server may disconnect clients that have sent no requests for
roughly 10 minutes.

**Signature**

> Function:
> server.ping()
> :::
>
> Version added:
> 1.2
> :::

**Result**

> Returns `null`{.interpreted-text role="const"}.

## server.version

Identify the client to the server and negotiate the protocol version.
Only the first `server.version`{.interpreted-text role="func"} message
is accepted.

**Signature**

> Function:
> server.version(client\_name=\"\", protocol\_version=\"1.4\")
>
> -   *client_name*
>
>     A string identifying the connecting client software.
>
> -   *protocol_version*
>
>     An array `[protocol_min, protocol_max]`, each of which is a
>     string. If `protocol_min` and `protocol_max` are the same, they
>     can be passed as a single string rather than as an array of two
>     strings, as for the default value.
>
> The server should use the highest protocol version both support:
>
>     version = min(client.protocol_max, server.protocol_max)
>
> If this is below the value:
>
>     max(client.protocol_min, server.protocol_min)
>
> then there is no protocol version in common and the server must close
> the connection. Otherwise it should send a response appropriate for
> that protocol version.

**Result**

> An array of 2 strings:
>
> > `[server_software_version, protocol_version]`
>
> identifying the server and the protocol version that will be used for
> future communication.

**Example**:

    server.version("Electron Cash 3.3.6", ["1.2", "1.4"])

**Example Result**:

    ["Rostrum 2.2.3", "1.4"]

## token.address.get\_balance

Return the confirmed and unconfirmed balances of tokens in a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.get\_balance(address)
>
> Version added: Rostrum 6.0
>
> -   *address*
>
>     The address as a Nexa or Cash Address string (with or without prefix).

**Result**

> See `token.scripthash.get_balance`.

## token.address.get\_history

Return the confirmed and unconfirmed token history of a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.get\_history(address, cursor *(optional)*, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>
> -   *cursor (optional)*
>
>     Null or string. If null, indicates that all histoy is requested, from newest entry first. If a cursor
>     is provided from an earlier call, history is continued from last transaction in earlier call.
>
> -   *token (optional)*
>
>     Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.


**Result**

> See `token.scripthash.get_history`

## token.address.get\_mempool

Return the unconfirmed token transactions of a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.get\_mempool(address, cursor *(optional)*, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Some server implementations may also support Legacy (base58)
>     addresses but are not required to do so by this specification.
>
> -   *cursor (optional)*
>
>     Null or string. If null, indicates that all histoy is requested, from newest entry first. If a cursor
>     is provided from an earlier call, history is continued from last transaction in earlier call.
>
> -   *token (optional)*
>
>     Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.

**Result**

> As for `token.scripthash.get_mempool`


## token.address.listunspent

Return an list of token UTXOs sent to a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.listunspent(address, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Some server implementations may also support Legacy (base58)
>     addresses but are not required to do so by this specification.
>
> -   *token (optional)*
>
>     Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.

**Result**

> As for `token.scripthash.listunspent`

## token.scripthash.get\_balance

Return the confirmed and unconfirmed balances of tokens in a `script hash`. Token is optional,
if provided, will filter on token.

**Signature**

> Function:
> token.scripthash.get\_balance(scripthash, cursor *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -    *scripthash*
>
>      The script hash as a hexadecimal string.
>
> -   *token (optional)*
>
>     Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.
>
> -   *cursor*
>
>     If the address has a large amount of tokens, the results may be paginated. The cursor is `null` if this result was the
>     last (or the full) result set. If there are more results to
>     fetch, call the method again with this cursor value to get next set of results.


**Result**

> Dictionary of keys `confirmed` and `unconfirmed`. The value of each is dict of tokens and
> the amount in given scripthash.

**Result Example**

    {
      "confirmed": {
        "3c469e9d6c5875d37a43f353d4f88e61fcf812c66eee3457465a40b0da4153e0": 103873966
      },
      "unconfirmed": {
        "3c469e9d6c5875d37a43f353d4f88e61fcf812c66eee3457465a40b0da4153e0": 236844
      },
      "cursor": "t1S7vY94La"
    }


## token.scripthash.get\_history

Return the confirmed and unconfirmed token history of a `script hash`

**Signature**

> Function:
> token.scripthash.get\_history(scripthash, cursor *(optional)*, token *(optional)*)
>
> Version added:
> Rostrum 6.0.
>
> *scripthash*
>
> > The script hash as a hexadecimal string.
>
> *cursor (optional)*
>
> > Null or string. If null, indicates that all histoy is requested, from newest entry first. If a cursor
> > is provided from an earlier call, history is continued from last transaction in earlier call.
>
> *token (optional)*
>
> > Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.
>

**Result**

> A dictionary with a cursor and a list of confirmed transactions in blockchain order, including mempool.
> Each confirmed transaction is an array with
> the following keys:
>
> -   *height*
>
>     The integer height of the block the transaction was confirmed in.
>
> -   *tx_hash*
>
>     The transaction hash in hexadecimal.
>
> See `token.scripthash.get_mempool`
> for how mempool transactions are returned.
>
> If the cursor is non-null, this denotes that the history retuned is incomplete and suggests the client call the same query again with cursor provided
> for continuation of history. If cursor is null, it denotes the latest history entry is provided in current response.

**Result Examples**

    {
      "transactions": [
      {
        "height": 200004,
        "tx_hash": "acc3758bd2a26f869fcc67d48ff30b96464d476bca82c1cd6656e7d506816412"
      },
      {
        "height": 215008,
        "tx_hash": "f3e1bf48975b8d6060a9de8884296abb80be618dc00ae3cb2f6cee3085e09403"
      {,
      {
        "fee": 20000,
        "height": 0,
        "tx_hash": "9fbed79a1e970343fcd39f4a2d830a6bde6de0754ed2da70f489d0303ed558ec"
      }
    ],
    "cursor": "t1S7vY94La"

## token.scripthash.get\_mempool

Return the unconfirmed token transactions of a `script hash`.

**Signature**

> Function:
> token.scripthash.get\_mempool(scripthash, cursor *(optional)*, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *scripthash*
>
>     The script hash as a hexadecimal string.
>
> -   *cursor (optional)*
>
>     Null or string. If null, indicates that all histoy is requested, from newest entry first. If a cursor
>     is provided from an earlier call, history is continued from last transaction in earlier call.
>
> -   *token (optional)*
>
> -   Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.

**Result**

> A dictionary with a transaction list of mempool transactions
> in arbitrary order. Each mempool transaction is a dictionary
> with the following keys:
>
> -   *height*
>
>     `0` if all inputs are confirmed, and `-1` otherwise.
>
> -   *tx_hash*
>
>     The transaction hash in hexadecimal.
>
> -   *fee*
>
>     The transaction fee in minimum coin units (satoshis).
>
> -   *cursor*
>
>     Null if it was the last (or the full) result set. If there are more results to
>     fetch, call the method again with this cursor value to get next set of results.

**Result Example**

    {
        "transactions": [
        {
            "tx_hash": "45381031132c57b2ff1cbe8d8d3920cf9ed25efd9a0beb764bdb2f24c7d1c7e3",
            "height": 0,
            "fee": 24310
        }
        ],
        "cursor": "t1S7vY94La"
    ]

## token.scripthash.listunspent

Return an list of token UTXOs sent to a script hash.

**Signature**

> Function:
> token.scripthash.listunspent(scripthash, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *scripthash*
>
>     The script hash as a hexadecimal string.
>
> -   *token (optional)*
>
> -   Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.

**Result**

> A list of unspent token outputs. This function takes the
> mempool into account. Mempool transactions paying to the address are
> included at the end of the list in an undefined order. Any output that
> is spent in the mempool does not appear. Each output is a dictionary
> with the following keys:
>
> -   *height*
>
>     The integer height of the block the transaction was confirmed in.
>     `0` if the transaction is in the mempool.
>
> -   *tx_pos*
>
>     The zero-based index of the output in the transaction's list of
>     outputs.
>
> -   *tx_hash*
>
>     The output's transaction hash as a hexadecimal string. The hash
>     is little-endian encoded (same as bitcoind RPC).
>
> -   *value*
>
>     The output's value in minimum coin units (satoshis).
>
> -   *token_id_hex*
>
>     The token identifier as a hexadecimal string
>
> -   *token_amount*
>
>     The token amount in utxo

**Result Example**

    [
      {
        "tx_pos": 0,
        "value": 546,
        "tx_hash": "9f2c45a12db0144909b5db269415f7319179105982ac70ed80d76ea79d923ebf",
        "height": 437146,
        "token_id_hex": "5c087b9f1824d38c14e42d84c7bb9502da4a63b81cdb9b49e71bbe3b46c83111",
        "token_amount": 42,
      },
      {
        "tx_pos": 0,
        "value": 546,
        "tx_hash": "3d2290c93436a3e964cfc2f0950174d8847b1fbe3946432c4784e168da0f019f",
        "height": 441696,
        "token_id_hex": "8f6bda419cc2181a201731973aa3f965d79ea10da7bf3c23fd9b61dbf9e3dca7",
        "token_amount": 11,
      }
    ]

## token.transaction.get_history

Return all confirmed and unconfirmed token transaction history of a given token.


**Signature**

> Function:
> token.transaction.get\_history(token, cursor *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *token*
>
> -   Token ID as cashaddr encoded or hexadecimal string.

**Result**

> A list of unconfirmed and confirmed transactions. Unconfirmed transactions are returned first in
> no specific order. Confirmed transactions are returned by latest confirmed first. Each transaction is a dictionary with
> the following keys:
>
> -   *height*
>
>     The integer height of the block the transaction was confirmed in.
>
> -   *tx_hash*
>
>     The transaction hash in hexadecimal.
>

**Result Examples**

    "transactions": [
      {
        "height": 0,
        "tx_hash": "9fbed79a1e970343fcd39f4a2d830a6bde6de0754ed2da70f489d0303ed558ec"
      },
      {
        "height": 200004,
        "tx_hash": "acc3758bd2a26f869fcc67d48ff30b96464d476bca82c1cd6656e7d506816412"
      },
      {
        "height": 215008,
        "tx_hash": "f3e1bf48975b8d6060a9de8884296abb80be618dc00ae3cb2f6cee3085e09403"
      },
    ]

## token.genesis.info

Info from token creation transaction.

**Signature**

> Function:
> token.genesis.info(token)
>
> Version added:
> Rostrum 6.0
>
> -   *token*
>
> -   Token ID as cashaddr encoded or hexadecimal string.

**Result Example**

    {
        "document_hash": "cef3a7be7f95f3c27f393c74c99a2c1f1f8c658d3cd973c5ff3c266c7133aa38",
        "document_url": "https://example.org",
        "height": 102,
        "name": "TEST",
        "ticker": "TEST",
        "token_id_hex": "ec898b9817c86b36757941e427c8c9562db92ad42ff68629e221efc4d15e0000",
        "txid": "503a3af8363d506c18d99efe538122947911038b5b4f8a0d62d6127e548bb13c",
        "txidem": "316862b66bf34ab6a7c24f6714e91ced836b50b540a5f40cf6f94499f14d322d"
    }
